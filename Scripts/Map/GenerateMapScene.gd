extends Node2D

@onready var main_map:MainMapGenerator = $MainMap


func _ready():
	var data:Dictionary = {
		"action" = MainMapGenerator.ACTION.GENERATE_MAP
	}
	main_map.init(data)
