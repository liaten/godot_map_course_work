class_name ConfirmExit
extends VBoxContainer
var main_map: MainMapGenerator


func init(_main_map:MainMapGenerator) ->void:
	main_map = _main_map


func _on_apply_pressed():
	main_map.show_main_menu()
