class_name NeighbourLabel
extends RichTextLabel


var id:int
var site_polygon:MapSite


func init(_site_polygon) ->void:
	site_polygon = _site_polygon


func _on_mouse_entered() ->void:
	tooltip_text = "Вывести информацию о ячейке " + text
	text = "[u]" + text + "[/u]"


func _on_mouse_exited() ->void:
	text = text.left(-4).right(-3)


func _input(event) ->void:
	if event.is_action_pressed("left_mouse"):
		if Rect2(Vector2(), size).has_point(get_local_mouse_position()):
			if(is_instance_valid(site_polygon)):
				site_polygon.select_site(id)
			return
