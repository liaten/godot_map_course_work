class_name Mineral

enum type{
	FOSSIL_FUEL, # Горючие полезные ископаемые
	ORE, # Руда
	BUILDING_MATERIAL, # Строительные материалы
	GEM, # Драгоценные камни
}

enum fossil_fuel{
	OIL, # Нефть
	COAL, # Уголь
	GAS # Природный газ
}

enum ore{
	URANIUM, # Уран
	ALUMINUM, # Алюминий
	SULFUR, # Сера
	COPPER, # Медь
	SILVER, # Серебро
	GOLD, # Золото
	PLATINUM # Платина
}

enum building_material{
	LIMESTONE, # Известняк
	SAND, # Песок
	CLAY, # Глина
	TREE # Древесина
}

enum gem{
	DIAMOND, # Алмаз
	EMERALD, # Изумруд
	RUBY, # Рубин
	SAPPHIRE # Сапфир
}

static func get_name_ru(type_value:int, mineral:int) -> String:
	match type_value:
		type.FOSSIL_FUEL:
			match mineral:
				fossil_fuel.OIL: # Нефть
					return "Нефть"
				fossil_fuel.COAL: # Уголь
					return "Уголь"
				fossil_fuel.GAS: # Природный газ
					return "Природный газ"
		type.ORE:
			match mineral:
				ore.URANIUM: # Уран
					return "Уран"
				ore.ALUMINUM: # Алюминий
					return "Алюминий"
				ore.SULFUR: # Сера
					return "Сера"
				ore.COPPER: # Медь
					return "Медь"
				ore.SILVER: # Серебро
					return "Серебро"
				ore.GOLD: # Золото
					return "Золото"
				ore.PLATINUM: # Платина
					return "Платина"
		type.BUILDING_MATERIAL:
			match mineral:
				building_material.LIMESTONE: # Известняк
					return "Известняк"
				building_material.SAND: # Песок
					return "Песок"
				building_material.CLAY: # Глина
					return "Глина"
				building_material.TREE: # Древесина
					return "Древесина"
		type.GEM:
			match mineral:
				gem.DIAMOND: # Алмаз
					return "Алмаз"
				gem.EMERALD: # Изумруд
					return "Изумруд"
				gem.RUBY: # Рубин
					return "Рубин"
				gem.SAPPHIRE: # Сапфир
					return "Сапфир"
	return "ОШИБКА"
