class_name LoadCivAs
extends Node2D

var screen_size_x: float = DisplayServer.screen_get_size().x
var screen_size_y: float = DisplayServer.screen_get_size().y
var map_file_name: String
var civ_file_name: String
var main_map:MainMapGenerator = null
var map_list:ItemList
var civ_list:ItemList
var color_rect:ColorRect
var main_menu:MainMenu
var civ_locations_hashes:Dictionary = {}

func init(data) ->void:
	if "main_menu" in data:
		self.main_menu = data.main_menu


func init_civ_hash(_civ_hash,civ_location):
	civ_locations_hashes[civ_location] = _civ_hash


func _ready() ->void:
	color_rect = get_node("ColorRect")
	color_rect.position = Vector2(screen_size_x/2-color_rect.offset_right/2,screen_size_y/2-color_rect.offset_bottom/2)
	map_list = color_rect.get_node("WorkPlace/Lists/MapList")
	civ_list = color_rect.get_node("WorkPlace/Lists/CivList")


func _on_clear_button_pressed() ->void:
	pass


func _on_clear_all_button_pressed() ->void:
	Files.clear_maps_dir()
	_on_close_button_pressed()
	if(main_map!=null):
		main_map.fast_load_button.disabled = true


func free_self():
	for child in self.get_children():
		if(is_instance_valid(child)):
			child.queue_free()
	if is_instance_valid(self):
		self.queue_free()


func _on_close_button_pressed() ->void:
	free_self()


func _on_load_button_pressed() ->void:
	if(main_map!=null):
		var data:Dictionary = {
			"load_map_location" = Files.map_folder + map_file_name
		}
		main_map.load_map_create_thread(data)
	else:
		var main_map_generator:MainMapGenerator = load("res://Scenes/Map/MainMapGenerator.tscn").instantiate()
		var root = get_tree().get_root()
		var main_game_node:Node2D = root.get_node("Main")
		main_game_node.add_child(main_map_generator)
		var data:Dictionary = {
			"action" = MainMapGenerator.ACTION.LOAD_MAP,
			"load_map_location" = Files.map_folder + map_file_name
		}
		main_map_generator.init(data)
	free_self()


func _on_item_list_item_selected(index) ->void:
	civ_list.clear()
	map_file_name = map_list.get_item_text(index)
	for civ_location in civ_locations_hashes.keys():
		var civ_map_hash = civ_locations_hashes[civ_location]
		if(civ_map_hash == Files.get_hash(Files.map_folder + map_file_name)):
			civ_list.add_item(civ_location)


func _on_tree_exited() ->void:
	if(main_menu!=null):
		main_menu.set_load_as_window_inactive()
	if(main_map!=null):
		main_map.allow_all()
		main_map.set_load_as_window_inactive()


func _on_tree_entered() ->void:
	if(get_tree().get_root().has_node("GenerateMapScene/MainMap")):
		main_map = get_tree().get_root().get_node("GenerateMapScene/MainMap")
	if(main_menu!=null):
		main_menu.set_load_as_window_active()
	if(main_map!=null):
		main_map.restrict_all()
		main_map.set_load_as_window_active()


func _on_civ_list_item_selected(index) ->void:
	civ_file_name = civ_list.get_item_text(index)


func _on_map_list_item_selected(index) ->void:
	civ_list.clear()
	map_file_name = map_list.get_item_text(index)
	for civ_location in civ_locations_hashes.keys():
		var civ_map_hash = civ_locations_hashes[civ_location]
		if(civ_map_hash == Files.get_hash(Files.map_folder + map_file_name)):
			civ_list.add_item(civ_location)
