class_name Civilization

var id:int
var taken_sites_ids: Array = []
var civs_in_war_against: Array = []
var alliances: Array = []
var war_sites: Array = []
var civ_name: String = ""
var civ_color: Color
var era_id:int


static func get_save_data_json(_data:Dictionary):
	var save_dict:Dictionary = {
		"civs":{},
		"civ_range":_data.civ_range,
		"map_hash":_data.map_hash
		}
	var civs = _data.civs
	var _civ = civs.values()[0]
	
	for civ in civs.values():
		save_dict["civs"][civ.id] = {
			"id" = civ.id,
			"taken_sites_ids" = civ.taken_sites_ids,
			"civs_in_war_against" = civ.civs_in_war_against,
			"alliances" = civ.alliances,
			"war_sites" = civ.war_sites,
			"civ_name" = civ.civ_name,
			"civ_color" = civ.civ_color.to_html()
		}
	
	return JSON.stringify(save_dict)


func _init(data:Dictionary):
	if("chosen_land" in data):
		taken_sites_ids.append(data.chosen_land)
	elif("taken_sites_ids" in data):
		taken_sites_ids = data.taken_sites_ids
	else:
		pass
	civ_name = data.chosen_name
	civ_color = data.chosen_color
	id = data.id


enum prefix_enum {
	AM,
	AUST,
	BRI,
	CAR,
	CEZ,
	CHE,
	COR,
	DE,
	EST,
	FIN,
	FRAN,
	GER,
	HAN,
	HIS,
	HUN,
	JAP,
	KI,
	LAT,
	MON,
	NOR,
	OM,
	POR,
	ROS,
	TUR,
	YAR,
	YUG
}


enum suffix_enum {
	AINE,
	AN,
	CIA,
	CHIA,
	CONY,
	DEN,
	DORIA,
	ERICA,
	ERIA,
	LAND,
	MANIA,
	NA,
	ONIA,
	PANIA,
	PORIA,
	RONIA,
	SIA,
	SY,
	TAIN,
	TUGAL,
	VIA,
	ZANIA,
	ZIA
}


static func get_prefix_ru(prefix_value:int) -> String:
	match prefix_value:
		prefix_enum.AM:
			return "Ам"
		prefix_enum.AUST:
			return "Авст"
		prefix_enum.BRI:
			return "Бри"
		prefix_enum.CAR:
			return "Кар"
		prefix_enum.CEZ:
			return "Цез"
		prefix_enum.CHE:
			return "Че"
		prefix_enum.COR:
			return "Кор"
		prefix_enum.DE:
			return "Де"
		prefix_enum.EST:
			return "Эст"
		prefix_enum.FIN:
			return "Фин"
		prefix_enum.FRAN:
			return "Фран"
		prefix_enum.GER:
			return "Гер"
		prefix_enum.HAN:
			return "Хан"
		prefix_enum.HIS:
			return "Ис"
		prefix_enum.HUN:
			return "Вен"
		prefix_enum.JAP:
			return "Яп"
		prefix_enum.KI:
			return "Ки"
		prefix_enum.LAT:
			return "Лат"
		prefix_enum.MON:
			return "Мон"
		prefix_enum.NOR:
			return "Нор"
		prefix_enum.OM:
			return "Ом"
		prefix_enum.POR:
			return "Пор"
		prefix_enum.ROS:
			return "Рос"
		prefix_enum.TUR:
			return "Тур"
		prefix_enum.YAR:
			return "Яр"
		prefix_enum.YUG:
			return "Юг"
	print("PREFIX ERROR ON INDEX: " + str(prefix_value))
	return ""


static func get_suffix_ru(suffix_value:int) -> String:
	match suffix_value:
		suffix_enum.AINE:
			return "ина"
		suffix_enum.AN:
			return "ан"
		suffix_enum.CIA:
			return "ция"
		suffix_enum.CHIA:
			return "хия"
		suffix_enum.CONY:
			return "кони"
		suffix_enum.DEN:
			return "ден"
		suffix_enum.DORIA:
			return "дория"
		suffix_enum.ERICA:
			return "ерика"
		suffix_enum.ERIA:
			return "эрия"
		suffix_enum.LAND:
			return "ландия"
		suffix_enum.MANIA:
			return "мания"
		suffix_enum.NA:
			return "на"
		suffix_enum.ONIA:
			return "ония"
		suffix_enum.PANIA:
			return "пания"
		suffix_enum.PORIA:
			return "пория"
		suffix_enum.RONIA:
			return "рония"
		suffix_enum.SIA:
			return "сия"
		suffix_enum.SY:
			return "си"
		suffix_enum.TAIN:
			return "тания"
		suffix_enum.TUGAL:
			return "тугалия"
		suffix_enum.VIA:
			return "вия"
		suffix_enum.ZANIA:
			return "зания"
		suffix_enum.ZIA:
			return "зия"
	print("SUFFIX ERROR ON INDEX: " + str(suffix_value))
	return ""


static func generate_civilization_name() -> String:
	var vow = "аеиоу"
	var con = "бвгдзклмнпрст"
	var rand_val_con = vow[randi() % vow.length()] + con[randi() % con.length()]
	return get_prefix_ru(randi() % (prefix_enum.size())) + rand_val_con + get_suffix_ru(randi() % (suffix_enum.size()))


static func generate_town_name() -> String:
	var vow = "аоеуи"
	var con_start = "бвгдзклмнпрстфхш"
	var con_end = "бвгджзклмнпрстфхцчшщь"
	var pattern = "^((["+con_start+"]{0,4})["+vow+"]+["+con_start+"]*){1}((["+con_start+"]{0,4})["+vow+"]?+["+con_end+"]*([ая]р|[иы]я|[вмлр]а|бург|град|ло|[мщз]ь){1})+$"
	var r = RegEx.new()
	var _regex = r.compile(pattern)
	var town_name = ""
	while !r.search(town_name):
		town_name = ""
		for i in range(randi_range(6, 12)):
			if i % 2 == 0:
				town_name += con_start[randi() % con_start.length()]
			else:
				if i % 4 == 0:
					town_name += con_end[randi() % con_end.length()]
				else:
					town_name += vow[randi() % vow.length()]
	return town_name.capitalize()


static func generate_province_name() -> String:
	var vow = "аеиоу"
	var con_start = "бвгдзклмнпрст"
	var con_end = "бвгджзклмнпрстфхцчшщь"
	var suffixes = "ми|си|ти|са|за|ра|ла"
	var pattern = "^((["+con_start+"]{0,4})["+vow+"]+["+con_start+"]*){1}((["+con_start+"]{0,4})["+vow+"]?+["+con_end+"]*([ая]р|[иы]я|[вмлр]а|бург|град|ло|[мщз]ь){1}("+suffixes+"){1})+$"
	var r = RegEx.new()
	var _regex = r.compile(pattern)
	var name = ""
	while !r.search(name):
		name = ""
		for i in range(randi_range(4, 12)):
			if i % 2 == 0:
				name += con_start[randi() % con_start.length()]
			else:
				if i % 4 == 0:
					name += con_end[randi() % con_end.length()]
				else:
					name += vow[randi() % vow.length()]
		name += ["ми", "си", "ти", "са", "за", "ра", "ла"][randi() % 7]
		
	return name.capitalize()


static func get_random_civ_color() -> Color:
	var r := randf_range(0, 1)
	var g := randf_range(0, 1)
	var b := randf_range(0, 1)
	return Color(r,g,b,0.4)
