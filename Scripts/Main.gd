class_name Main
extends Node2D

var screen_size_x: float = DisplayServer.screen_get_size().x
var screen_size_y: float = DisplayServer.screen_get_size().y
var main_menu_resource:Resource = preload("res://Scenes/Menu/MainMenu.tscn")


func _ready() ->void:
	add_main_menu()


func add_main_menu() ->void:
	var main_menu: ColorRect = main_menu_resource.instantiate()
	add_child(main_menu)
	main_menu.position = Vector2(screen_size_x/2 - main_menu.size.x/2, screen_size_y/2 - main_menu.size.y/2)
	if(Files.check_existing_file(Files.get_fast_map_location())):
		var generate_map_check_box:CheckBox = main_menu.get_node("Generate_Map_Button/GenerateMapCheckBox")
		generate_map_check_box.set_pressed(true)
