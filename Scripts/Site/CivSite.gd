class_name CivSite
extends Polygon2D


var main_map:MainMapGenerator
var restricted_zones
var iteration:int
var voronoi_site: Delaunay_Bower.VoronoiSite
var layer_to_not_move_items
var civ:Civilization


func init(data:Dictionary) ->void:
	iteration = data.iteration
	layer_to_not_move_items = data.layer_to_not_move_items
	voronoi_site = data.site
	civ = data.civ
	color = civ.civ_color
	set_polygon(voronoi_site.polygon)
