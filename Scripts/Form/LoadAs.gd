class_name LoadAs
extends Node2D

var screen_size_x: float = DisplayServer.screen_get_size().x
var screen_size_y: float = DisplayServer.screen_get_size().y
var load_text:String
var main_map:MainMapGenerator = null
var item_list:ItemList
var color_rect:ColorRect
var main_menu:MainMenu
var last_item_index = null
var action:MainMapGenerator.ACTION

func init(data) ->void:
	if "main_menu" in data:
		self.main_menu = data.main_menu
	self.action = data.action
	print(MainMapGenerator.ACTION.keys()[action])

func _ready() ->void:
	color_rect = get_node("ColorRect")
	color_rect.position = Vector2(screen_size_x/2-color_rect.offset_right/2,screen_size_y/2-color_rect.offset_bottom/2)
	item_list = color_rect.get_node("WorkPlace/ItemList")


func _on_clear_button_pressed() ->void:
	if(last_item_index==null):
		return
	item_list.remove_item(last_item_index)
	Files.remove_user_file(Files.map_folder + load_text)


func _on_clear_all_button_pressed() ->void:
	Files.clear_maps_dir()
	if(main_map!=null):
		main_map.fast_load_button.disabled = true
	item_list.clear()


func free_self():
	for child in self.get_children():
		if(is_instance_valid(child)):
			child.queue_free()
	if is_instance_valid(self):
		self.queue_free()


func _on_close_button_pressed() ->void:
	free_self()


func _on_load_button_pressed() ->void:
	if(main_map!=null):
		var data:Dictionary = {
			"load_map_location" = Files.map_folder + load_text
		}
		main_map.load_map_create_thread(data)
	else:
		var main_map_generator:MainMapGenerator = load("res://Scenes/Map/MainMapGenerator.tscn").instantiate()
		var root = get_tree().get_root()
		var main_game_node:Node2D = root.get_node("Main")
		main_game_node.add_child(main_map_generator)
		var data:Dictionary = {
			"action" = MainMapGenerator.ACTION.LOAD_MAP,
			"load_map_location" = Files.map_folder + load_text
		}
		main_map_generator.init(data)
	free_self()


func _on_item_list_item_selected(index) ->void:
	load_text = item_list.get_item_text(index)
	last_item_index = index


func _on_tree_exited() ->void:
	if(main_menu!=null):
		main_menu.set_load_as_window_inactive()
		return
	if(main_map!=null):
		main_map.allow_all()
		main_map.set_load_as_window_inactive()


func _on_tree_entered() ->void:
	if(get_tree().get_root().has_node("GenerateMapScene/MainMap")):
		main_map = get_tree().get_root().get_node("GenerateMapScene/MainMap")
	if(main_menu!=null):
		main_menu.set_load_as_window_active()
		return
	if(main_map!=null):
		main_map.restrict_all()
		main_map.set_load_as_window_active()
