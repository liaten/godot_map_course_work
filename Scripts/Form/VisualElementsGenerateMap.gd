class_name VisualElementsGenerateMap
extends Node

var main_map:MainMapGenerator

@onready var selectable_settings_control:Control = $ColorRect/SelectableSettings
@onready var base_settings_control:BaseSettings = $ColorRect/BaseSettings
@onready var map_settings:VBoxContainer = $ColorRect/SelectableSettings/MapSettings
@onready var color_rect:ColorRect = $ColorRect


var settings_view_selector:OptionButton

func init(_main_map) ->void:
	main_map = _main_map


func _ready():
	base_settings_control.init(main_map,map_settings)
	color_rect.size = Vector2(
		color_rect.size.x,
		base_settings_control.size.y + map_settings.size.y,
	)


func hide() ->void:
	for child in self.get_children():
		child.hide()


func show() ->void:
	for child in self.get_children():
		child.show()
