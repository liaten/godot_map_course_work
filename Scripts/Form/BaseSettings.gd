class_name BaseSettings
extends Control
var main_map:MainMapGenerator
var map_settings:MapSettings


func init(_main_map:MainMapGenerator,_map_settings):
	main_map = _main_map
	map_settings = _map_settings


func _on_create_button_pressed():
	main_map.regenerate(main_map.ACTION.GENERATE_MAP)


func _on_save_button_pressed():
	main_map.save_map_create_thread()
	main_map.fast_load_button.disabled = false


func _on_load_as_button_pressed():
	main_map.create_load_as_window(main_map.ACTION.LOAD_MAP)


func _on_load_button_pressed():
	main_map.load_map_create_thread()


func _on_exit_button_pressed():
	main_map.escape_action_checker()


func _on_next_button_pressed():
	main_map.continue_action_checker()


func _on_create_from_ve_button_pressed():
	var data:Dictionary = {}
	data.points_seed = int(map_settings.points_seed_slider.get_value())
	data.seed_e = int(map_settings.elevation_seed_slider.get_value())
	data.seed_m = int(map_settings.moisture_seed_slider.get_value())
	
	data.elevation = {}
	data.moisture = {}
	
	data.elevation.fractal_octaves = int(map_settings.elevation_octaves_slider.get_value())
	data.elevation.lacunarity = map_settings.elevation_lacunarity_slider.get_value()
	data.elevation.frequency = map_settings.elevation_frequency_slider.get_value()
	data.elevation.fractal_gain = map_settings.elevation_fractal_gain_slider.get_value()
	
	data.moisture.fractal_octaves = int(map_settings.moisture_octaves_slider.get_value())
	data.moisture.lacunarity = map_settings.moisture_lacunarity_slider.get_value()
	data.moisture.frequency = map_settings.moisture_frequency_slider.get_value()
	data.moisture.fractal_gain = map_settings.moisture_fractal_gain_slider.get_value()
	
	main_map.regenerate_from_ve(data)


func _on_save_as_button_pressed():
	main_map.create_save_as_window(main_map.ACTION.SAVE_MAP)


func _on_spin_box_value_changed(value):
	main_map.change_main_map_range(value)


func _on_settings_view_selector_item_selected(index):
	pass # Replace with function body.
