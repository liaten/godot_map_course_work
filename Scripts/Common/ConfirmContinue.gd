class_name ConfirmContinue
extends VBoxContainer
var main_map: MainMapGenerator
var confirm:Confirm
var current_action:MainMapGenerator.ACTION


func init(_main_map:MainMapGenerator,_confirm:Confirm,_current_action:MainMapGenerator.ACTION) ->void:
	main_map = _main_map
	confirm = _confirm
	current_action = _current_action


func _on_apply_pressed():
	confirm.hide()
	if(current_action in [MainMapGenerator.ACTION.GENERATE_MAP,MainMapGenerator.ACTION.LOAD_MAP]):
		main_map.show_main_menu_save_map()
		return
	if(current_action in [MainMapGenerator.ACTION.GENERATE_CIVILIZATIONS,MainMapGenerator.ACTION.LOAD_CIVILIZATIONS]):
		main_map.show_main_menu_save_civilizations()
		return
