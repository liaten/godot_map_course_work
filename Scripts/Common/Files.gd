class_name Files

#const fast_map_location = "res://_map/fast_saved_map.save"
#const fast_civ_location = "res://_civ/fast_saved_map.save"
#const user_folder = "res://"
#const civ_folder = "res://_civ/"
#const map_folder = "res://_map/"

const user_folder = "user://"
const civ_folder = "user://civ/"
const map_folder = "user://map/"
const fast_map_location = "user://map/fast_saved_map.save"
const fast_civ_location = "user://civ/fast_saved_civ.save"

const CHUNK_SIZE = 1024


static func get_fast_map_location() ->String:
	return fast_map_location


static func get_fast_civ_location() ->String:
	return fast_civ_location


static func list_files_in_directory(path) ->Array:
	var files:Array = []
	var dir = DirAccess.open(path)
	dir.list_dir_begin()
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with("."):
			files.append(file)
	dir.list_dir_end()
	return files


static func get_civilizations_saves() ->Array:
	return list_files_in_directory(civ_folder)


static func get_civilizations_saves_by_map(map) ->Array:
	var all_civs:Array = list_files_in_directory(civ_folder)
	if(len(all_civs)==0):
		return []
	var civ_saves_by_map:Array = []
	for load_civ_location in all_civs:
		var file = FileAccess.open(Files.civ_folder + load_civ_location, FileAccess.READ)
		var json_file = JSON.new()
		json_file.parse(file.get_line())
		var civ_data = json_file.get_data()
		if(civ_data.map_hash != Files.get_hash(map)):
			continue
		civ_saves_by_map.append(load_civ_location)
	return civ_saves_by_map


static func get_map_saves() ->Array:
	return list_files_in_directory(map_folder)


static func check_user_map_dir() ->void:
	var directory:DirAccess = DirAccess.open(user_folder)
	if(!directory.dir_exists(map_folder)):
		var _result = directory.make_dir_recursive(map_folder)


static func check_user_civ_dir() ->void:
	var directory:DirAccess = DirAccess.open(user_folder)
	if(!directory.dir_exists(civ_folder)):
		var _result = directory.make_dir_recursive(civ_folder)


static func check_existing_file(location) ->bool:
	if not FileAccess.file_exists(location):
		print("Файл " + location + " не существует")
		return false
	return true


static func remove_user_file(location) ->void:
	var directory = DirAccess.open(user_folder)
	directory.remove(location)


static func clear_maps_dir() ->void:
	for file in get_map_saves():
		remove_user_file(map_folder + file)


static func clear_civ_dir() ->void:
	for file in get_civilizations_saves():
		remove_user_file(civ_folder + file)


static func get_hash(path) ->String:
	# Check that file exists.
	if not FileAccess.file_exists(path):
		return ""
	# Start a SHA-256 context.
	var ctx = HashingContext.new()
	ctx.start(HashingContext.HASH_SHA256)
	# Open the file to hash.
	var file = FileAccess.open(path, FileAccess.READ)
	# Update the context after reading each chunk.
	while not file.eof_reached():
		ctx.update(file.get_buffer(CHUNK_SIZE))
	# Get the computed hash.
	var res = ctx.finish()
	
	# Return the result as hex string and array.
	return res.hex_encode()
