class_name VisualElementsGenerateCivilizations
extends Node

var main_map:MainMapGenerator
var map_location:String
var civs_locations:Array
@onready var load_button:Button = $ColorRect/LoadButton
@onready var load_as_button:Button = $ColorRect/LoadAsButton


func init(_main_map:MainMapGenerator,_map_location:String) ->void:
	main_map = _main_map
	map_location = _map_location
	recalculate_civs_locations()


func recalculate_civs_locations() ->void:
	civs_locations = Files.get_civilizations_saves_by_map(map_location)
	if(load_button!=null && load_as_button!=null):
		if(len(civs_locations) == 0):
			disable_load_buttons()
		else:
			enable_load_buttons()


func _ready():
	if(len(civs_locations) == 0):
		disable_load_buttons()


func disable_load_buttons() ->void:
	load_button.disabled = true
	load_as_button.disabled = true


func enable_load_buttons() ->void:
	load_button.disabled = false
	load_as_button.disabled = false


func _on_LineEditRange_text_changed(new_text) ->void:
	main_map.change_civs_range(new_text)


func _on_Create_Button_pressed() ->void:
	main_map.regenerate(main_map.ACTION.GENERATE_CIVILIZATIONS)


func _on_Save_Button_pressed() ->void:
	main_map.save_civs_create_thread({'do_recalculate_civs':true})


func _on_Load_Button_pressed() ->void:
	var civ_saves = Files.get_civilizations_saves_by_map(map_location)
	main_map.load_civ_create_thread()


func _on_Exit_Button_pressed() ->void:
	main_map.escape_action_checker()


func _on_NextButton_pressed() ->void:
	main_map.continue_action_checker()


func hide() ->void:
	for child in self.get_children():
		child.hide()


func show() ->void:
	for child in self.get_children():
		child.show()


func _on_CreateFromVEButton_pressed() ->void:
	var data:Dictionary = {}
	data.elevation = {}
	data.moisture = {}
	main_map.regenerate_from_ve(data)


func _on_SaveAsButton_pressed() ->void:
	main_map.create_save_as_window(main_map.ACTION.SAVE_CIVILIZATIONS)


func _on_LoadAsButton_pressed() ->void:
	main_map.create_load_as_window(main_map.ACTION.LOAD_CIVILIZATIONS)


func _on_spin_box_value_changed(value):
	main_map.change_civs_range(value)
