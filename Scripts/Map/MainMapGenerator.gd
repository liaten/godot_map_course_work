class_name MainMapGenerator
extends Node2D

@onready var active_camera:Camera2D = $ActiveCamera
@onready var non_movable_layer:CanvasLayer = $LayerToNotMoveItems

# Размер экрана в пикселях по каждой из осей
var screen_size_x: float = DisplayServer.screen_get_size().x
var screen_size_y: float = DisplayServer.screen_get_size().y

var biome_class:Biome = Biome.new()
var delone: Delaunay_Bower = null
var are_visuals_hidden: bool = true
var time_now: int = 0
var time_start: int = 0

const camera_movement_timeout:float = 0.005
const camera_movement_offset:int = 7

var loaded_map_location:String
var move_cam_func_thread_dict: Dictionary
var key_pressed_dict: Dictionary
var voronoi: Dictionary
var voronoi_lands_ids: Array = []
var voronoi_inhabitable_lands_ids: Array = []
var voronoi_inhabitable_lands_chosen_ids: Array = []
var civs_picked_colors: Array = []
var civs_picked_names: Array = []
var voronoi_inhabitable_lands_left_ids:Array = []
var noise_data: Dictionary
var ranges: Dictionary = {
	"map_range" = 512,
	"civ_range" = 4
}
var civs: Dictionary = {}

# Объект OpenSimplexNoise, отвечающий за генерацию высот и влажностей карты с помощью шума
var genE: FastNoiseLite = FastNoiseLite.new()
var genM: FastNoiseLite = FastNoiseLite.new()

var visual_elements
var spin_box:SpinBox
var notifications: Notifications
var notifications_container: VBoxContainer
var notifications_main_rect: ColorRect
var notifications_main_rect_pool_vector: PackedVector2Array
var visual_elements_main_rect_pool_vector: PackedVector2Array
var restricted_zones


var points_seed_slider: HSlider
var elevation_seed_slider: HSlider
var moisture_seed_slider: HSlider

var elevation_octaves_slider: HSlider
var elevation_frequency_slider: HSlider
var elevation_fractal_gain_slider: HSlider
var elevation_lacunarity_slider: HSlider

var moisture_octaves_slider: HSlider
var moisture_frequency_slider: HSlider
var moisture_fractal_gain_slider: HSlider
var moisture_lacunarity_slider: HSlider

var fast_save_button: Button
var fast_load_button: Button

var voronoi_gen_thread_pool:Array = []
var loading_screen_thread_pool: Array = []
var voronoi_load_thread_pool:Array = []
var voronoi_save_thread_pool:Array = []
var civ_save_thread_pool:Array = []
var civ_gen_thread_pool:Array = []
var civ_load_thread_pool:Array = []

var loading_screen: Node
var progress_bar: ProgressBar
var progress_bar_description: Label

var generations:Dictionary = {
	"map" = 0,
	"civ" = 0
}

var is_space_enter_action_restricted: bool = false
var is_scrolling_restricted: bool = false
var is_moving_map_restricted: bool = false
var is_hide_button_action_restricted: bool = false
var is_load_as_window_active: bool = false
var is_save_as_window_active: bool = false
var is_selecting_site_restricted: bool = false

var closable_windows_array:Array = []
var current_action: ACTION

var site_to_civ:Array = []

var map_site_polygon_resource:Resource = preload("res://Scenes/Site/MapSitePolygon.tscn")
var civ_site_polygon_resource:Resource = preload("res://Scenes/Site/CivSitePolygon.tscn")
var main_packed:PackedScene = ResourceLoader.load("res://Scenes/Main.tscn")

enum THREAD_STATUS{
	LOAD,
	SAVE,
	CREATE
}

enum ACTION{
	GENERATE_MAP,
	LOAD_MAP,
	SAVE_MAP,
	GENERATE_CIVILIZATIONS,
	LOAD_CIVILIZATIONS,
	SAVE_CIVILIZATIONS,
	GAME_OBSERVER,
	GAME_PLAYER
}

enum DIRECTION{
	RIGHT,
	DOWN,
	LEFT,
	TOP
}


enum MAP_VIEW{
	BIOME,
	HEIGHT,
	MOISTURE
}


func move_camera_thread(direction:DIRECTION, run:bool = true) ->void:
	if(run):
		key_pressed_dict[direction] = true
		move_cam_func_thread_dict[direction] = Thread.new()
		move_cam_func_thread_dict[direction].start(Callable(self, "move_camera").bind(direction))
		return
	key_pressed_dict[direction] = false
	move_cam_func_thread_dict[direction].wait_to_finish()


func move_camera(direction:DIRECTION) ->void:
	match direction:
		DIRECTION.DOWN:
			while(key_pressed_dict[direction]):
				var offset = active_camera.get_offset()
				var zoom = active_camera.get_zoom().x
				active_camera.set_offset(Vector2(offset.x,offset.y+camera_movement_offset / zoom))
				await get_tree().create_timer(camera_movement_timeout).timeout
		DIRECTION.TOP:
			while(key_pressed_dict[direction]):
				var offset = active_camera.get_offset()
				var zoom = active_camera.get_zoom().x
				active_camera.set_offset(Vector2(offset.x,offset.y-camera_movement_offset / zoom))
				await get_tree().create_timer(camera_movement_timeout).timeout
		DIRECTION.LEFT:
			while(key_pressed_dict[direction]):
				var offset = active_camera.get_offset()
				var zoom = active_camera.get_zoom().x
				active_camera.set_offset(Vector2(offset.x-camera_movement_offset / zoom,offset.y))
				await get_tree().create_timer(camera_movement_timeout).timeout
		DIRECTION.RIGHT:
			while(key_pressed_dict[direction]):
				var offset = active_camera.get_offset()
				var zoom = active_camera.get_zoom().x
				active_camera.set_offset(Vector2(offset.x+camera_movement_offset / zoom,offset.y))
				await get_tree().create_timer(camera_movement_timeout).timeout


func noiseE(nx,ny) ->float:
	return genE.get_noise_2d(nx,ny) / 2.0 + 0.5


func noiseM(nx,ny) ->float:
	return genM.get_noise_2d(nx,ny) / 2.0 + 0.5


func set_custom_noise_data(data:Dictionary) ->void:
	# Точки
	seed(data.points_seed)
	
	# Высота
	genE.seed = data.seed_e
	genE.fractal_octaves = data.elevation.fractal_octaves
	genE.fractal_gain = data.elevation.fractal_gain
	genE.fractal_lacunarity = data.elevation.lacunarity
	genE.frequency = data.elevation.frequency
	
	# Влажность
	genM.seed = data.seed_m
	genM.fractal_octaves = data.moisture.fractal_octaves
	genM.fractal_gain = data.moisture.fractal_gain
	genM.fractal_lacunarity = data.moisture.lacunarity
	genM.frequency = data.moisture.frequency


# Установка свойств генератора цивилизаций
func randomize_civ_data() ->void:
	var SEED: int = randi()
	seed(SEED)


# Установка свойств генератора карты и карт шумов
func randomize_map_noise_data() ->void:
	var SEED: int = randi()
	var SEED_E: int = randi()
	var SEED_M: int = randi()
	
	# Устанавливаем генератор случайных чисел в положение "неслучаности"
	seed(SEED)
	
	points_seed_slider.set_value(SEED)
	points_seed_slider.tooltip_text = str(SEED)
	
	noise_data.points_seed = SEED
	
	noise_data.elevation = {}
	noise_data.moisture = {}
	
	# Высота
	genE.seed = SEED_E
	
	elevation_seed_slider.set_value(SEED_E)
	elevation_seed_slider.tooltip_text = str(SEED_E)
	noise_data.elevation.seed = SEED_E
	
	var elevation_octaves = 9
	genE.fractal_octaves = elevation_octaves
	
	elevation_octaves_slider.set_value(elevation_octaves)
	elevation_octaves_slider.tooltip_text = str(elevation_octaves)
	noise_data.elevation.fractal_octaves = elevation_octaves
	
	var elevation_frequency = 0.007
	genE.frequency = elevation_frequency
	elevation_frequency_slider.set_value(elevation_frequency)
	elevation_frequency_slider.tooltip_text = str(elevation_frequency)
	noise_data.elevation.frequency = elevation_frequency
	
	var elevation_lacunarity = screen_size_x/700
	genE.fractal_lacunarity = elevation_lacunarity

	elevation_lacunarity_slider.set_value(elevation_lacunarity)
	elevation_lacunarity_slider.tooltip_text = str(elevation_lacunarity)
	noise_data.elevation.fractal_lacunarity = elevation_lacunarity
	
	var elevation_fractal_gain = 0.8
	genE.fractal_gain = elevation_fractal_gain

	elevation_fractal_gain_slider.set_value(elevation_fractal_gain)
	elevation_fractal_gain_slider.tooltip_text = str(elevation_fractal_gain)
	noise_data.elevation.fractal_gain = elevation_lacunarity
	
	# Влажность
	genM.seed = SEED_M
	
	moisture_seed_slider.set_value(SEED_M)
	moisture_seed_slider.tooltip_text = str(SEED_M)
	noise_data.moisture.seed = SEED_M
	
	var moisture_octaves = 9
	genM.fractal_octaves = moisture_octaves
	
	moisture_octaves_slider.set_value(moisture_octaves)
	moisture_octaves_slider.tooltip_text = str(moisture_octaves)
	noise_data.moisture.fractal_octaves = moisture_octaves
	
	var moisture_frequency = 0.007
	genM.frequency = moisture_frequency
	moisture_frequency_slider.set_value(moisture_frequency)
	moisture_frequency_slider.tooltip_text = str(moisture_frequency)
	noise_data.moisture.frequency = moisture_frequency
	
	var moisture_fractal_gain = 0.8
	genM.fractal_gain = moisture_fractal_gain

	moisture_fractal_gain_slider.set_value(moisture_fractal_gain)
	moisture_fractal_gain_slider.tooltip_text = str(moisture_fractal_gain)
	noise_data.moisture.fractal_gain = moisture_fractal_gain
	
	var moisture_lacunarity = screen_size_x/800
	genM.fractal_lacunarity = moisture_lacunarity

	moisture_lacunarity_slider.set_value(moisture_lacunarity)
	moisture_lacunarity_slider.tooltip_text = str(moisture_lacunarity)
	noise_data.moisture.fractal_lacunarity = moisture_lacunarity


func get_elevation(voronoi_val: Dictionary) -> Dictionary:
	var e: Dictionary = {} 
	var e_min = null # float
	var e_max = null # float
	
	for site in voronoi_val.values():
		var site_center = site.center
		var x = site_center.x
		e[x] = {}
		
	for site in voronoi_val.values():
		var site_center = site.center
		var x = site_center.x
		var y = site_center.y
		
		var nx1 = (x/screen_size_x) - 0.5
		var ny1 = (y/screen_size_y) - 0.5
		
		# https://www.redblobgames.com/maps/terrain-from-noise/#elevation-redistribution
		var e1 = noiseE(nx1,ny1) + 0.44 * noiseE(2*nx1,2*ny1) + 0.21 * noiseE(4*nx1,4*ny1) + 0.1 * noiseE(8*nx1,8*ny1) + 0.04 * noiseE(16*nx1,16*ny1)
		e1 /= (1+0.44+0.21+0.1+0.04)
		if(e_min == null):
			e_min = e1
			e_max = e1
		elif(e1>e_max):
			e_max = e1
		elif(e1<e_min):
			e_min = e1
		
		e[x][y] = e1
	
	var e_range = e_max - e_min
	
	for site in voronoi_val.values():
		var site_center = site.center
		var x = site_center.x
		var y = site_center.y
		
		e[x][y] = (e[x][y] - e_min) / e_range
		e[x][y] = pow(e[x][y],1.7)
		
		var nx = (2*x)/screen_size_x - 1
		var ny = (2*y)/screen_size_y - 1
		var nx2 = pow(nx,2)
		var ny2 = pow(ny,2)
		var d = 1 - ((1-nx2)*(1-ny2))
		e[x][y] = (e[x][y]+(1-d))/2 # Square Bump
	
	return e


func get_moisture(voronoi_val: Dictionary) -> Dictionary:
	var m: Dictionary = {} 
	var m_min = null # float
	var m_max = null # float
	
	for site in voronoi_val.values():
		var site_center = site.center
		var x = site_center.x
		m[x] = {}
	
	for site in voronoi_val.values():
		var site_center = site.center
		var x = site_center.x
		var y = site_center.y
		
		var nx1 = (x/screen_size_x) - 0.5
		var ny1 = (y/screen_size_y) - 0.5
		# https://www.redblobgames.com/maps/terrain-from-noise/#elevation-redistribution
		var m1 = noiseM(nx1,ny1) + 0.44 * noiseM(2*nx1,2*ny1) + 0.21 * noiseM(4*nx1,4*ny1) + 0.1 * noiseM(8*nx1,8*ny1) + 0.04 * noiseM(16*nx1,16*ny1)
		m1 /= (1+0.44+0.21+0.1+0.04)
		if(m_min == null):
			m_min = m1
			m_max = m1
		elif(m1>m_max):
			m_max = m1
		elif(m1<m_min):
			m_min = m1
		
		m[x][y] = m1
	
	var m_range = m_max - m_min
	
	for site in voronoi_val.values():
		var site_center = site.center
		var x = site_center.x
		var y = site_center.y
		m[x][y] = (m[x][y] - m_min) / m_range
	
	return m


func clear_civ_polygons(data:Dictionary) ->void:
	var iter = data.civ_site_generations
#	var site_count = 0
#
#	for child in self.get_children():
#		if child is CivSite && (child.iteration==iter):
#			site_count+=1
	
	for child in self.get_children():
		if child is CivSite && (child.iteration==iter):
			var site:CivSite = child
			if(is_instance_valid(site)):
				site.queue_free()


func clear_map_polygons(data:Dictionary) ->void:
	var iter = data.map_site_generations
	var thread_status = data.thread_status
	var site_count = 0
	
	for child in self.get_children():
		if child is MapSite && (child.iteration==iter):
			site_count+=1
	
	match thread_status:
		THREAD_STATUS.LOAD:
			for child in self.get_children():
				if child is MapSite && (child.iteration==iter):
					var progress_bar_value = (1.0 / float(site_count) * 20.0)
					progress_bar.set_value(float(progress_bar.get_value()) + float(progress_bar_value))
					var site:MapSite = child
					if(is_instance_valid(site)):
						site.queue_free()
		THREAD_STATUS.CREATE:
			for child in self.get_children():
				if child is MapSite && (child.iteration==iter):
					var progress_bar_value = (1.0 / float(site_count) * 33.3)
					progress_bar.set_value(float(progress_bar.get_value()) + float(progress_bar_value))
					var site:MapSite = child
					if(is_instance_valid(site)):
						site.queue_free()


func disable_polygons_highlight() ->void:
	for child in self.get_children():
		if child is MapSite:
			child.restrict_showing_polygon_line()


func enable_polygons_highlight() ->void:
	for child in self.get_children():
		if child is MapSite:
			child.allow_showing_polygon_line()


func create_loading_screen(action:ACTION) ->void:
	loading_screen = load("res://Scenes/Common/Loading.tscn").instantiate()
	non_movable_layer.add_child(loading_screen)
	progress_bar = loading_screen.get_node("ColorRect/ProgressBar")
	progress_bar_description = progress_bar.get_node("Description")
	var progress_bar_label:Label = progress_bar.get_node("Label")
	match action:
		ACTION.GENERATE_MAP:
			progress_bar_label.text = "Генерация карты"
		ACTION.LOAD_MAP:
			progress_bar_label.text = "Загрузка карты"
		ACTION.SAVE_MAP:
			progress_bar_label.text = "Сохранение карты"
		ACTION.GENERATE_CIVILIZATIONS:
			progress_bar_label.text = "Генерация цивилизаций"
		ACTION.LOAD_CIVILIZATIONS:
			progress_bar_label.text = "Загрузка цивилизаций"
		ACTION.SAVE_CIVILIZATIONS:
			progress_bar_label.text = "Сохранение цивилизаций"


func clear_loading_screen() ->void:
	loading_screen.get_parent().remove_child(loading_screen)
	if(is_instance_valid(loading_screen)):
		loading_screen.queue_free()


func show_loading_screen_generate_civ(ids:Dictionary) ->void:
	create_loading_screen(ACTION.GENERATE_CIVILIZATIONS)
	var civ_gen_thread_id = ids.civ_gen_thread_id
	var _civ_gen_thread_result = civ_gen_thread_pool[civ_gen_thread_id].start(Callable(self, "generate_civ").bind(ids))


func show_loading_screen_generate_map(ids:Dictionary) ->void:
	create_loading_screen(ACTION.GENERATE_MAP)
	var voronoi_gen_thread_id = ids.voronoi_gen_thread_id
	var _voronoi_gen_thread_result = voronoi_gen_thread_pool[voronoi_gen_thread_id].start(Callable(self, "generate_voronoi_map").bind(ids))


func show_loading_screen_load_civ(data:Dictionary) ->void:
	var civ_load_thread_id = data.civ_load_thread_id
	var civ_load_thread:Thread = civ_load_thread_pool[civ_load_thread_id]
	
	create_loading_screen(ACTION.LOAD_CIVILIZATIONS)
	var _civ_load_thread_result = civ_load_thread.start(Callable(self, "load_civ").bind(data))


func show_loading_screen_load_map(data:Dictionary) ->void:
	var voronoi_load_thread_id = data.voronoi_load_thread_id
	var voronoi_load_thread:Thread = voronoi_load_thread_pool[voronoi_load_thread_id]
	
	create_loading_screen(ACTION.LOAD_MAP)
	var _voronoi_load_thread_result = voronoi_load_thread.start(Callable(self, "load_map").bind(data))


func show_loading_screen_save_civs(data:Dictionary) ->void:
	var civs_save_thread_id = data.civ_save_thread_id
	var civs_save_thread:Thread = civ_save_thread_pool[civs_save_thread_id]
	
	create_loading_screen(ACTION.SAVE_CIVILIZATIONS)
	var _civs_save_thread_result = civs_save_thread.start(Callable(self, "save_civs").bind(data))



func show_loading_screen_save_map(data:Dictionary) ->void:
	var voronoi_save_thread_id = data.voronoi_save_thread_id
	var voronoi_save_thread:Thread = voronoi_save_thread_pool[voronoi_save_thread_id]
	
	create_loading_screen(ACTION.SAVE_MAP)
	var _voronoi_save_thread_result = voronoi_save_thread.start(Callable(self, "save_map").bind(data))


func regenerate(action:ACTION) ->void:
	match action:
		ACTION.GENERATE_MAP:
			randomize_map_noise_data()
			generate_voronoi_map_create_thread()
			fast_save_button.disabled = false
			fast_load_button.disabled = false
#			get_tree().reload_current_scene()
		ACTION.GENERATE_CIVILIZATIONS:
			randomize_civ_data()
			generate_civs_create_thread()
	


func regenerate_from_ve(data:Dictionary) ->void:
	set_custom_noise_data(data)
	generate_voronoi_map_create_thread()


func start_timer() ->void:
	if !are_visuals_hidden:
		time_start = Time.get_ticks_msec()


func stop_timer() ->void:
	if !are_visuals_hidden:
		time_now = Time.get_ticks_msec()
		var time_elapsed = time_now - time_start
		non_movable_layer.get_node("Visuals/ColorRect/BaseSettings/Time_Elapsed").text = "Время выполнения:\n" + str(time_elapsed) + " ms"


func save_civs(data:Dictionary = {}) ->void:
	call_deferred("start_timer")
	var saved_civs = FileAccess.open(data.save_civs_location, FileAccess.WRITE)
	var to_save_data = {
		"civs" = civs,
		"civ_range" = ranges.civ_range,
		"map_hash" = Files.get_hash(Files.get_fast_map_location())
	}
	var save_data:String = Civilization.get_save_data_json(to_save_data)
	saved_civs.store_line(save_data)
	saved_civs.close()
	call_deferred("stop_timer")
	call_deferred("civs_save_thread_done",data)


func save_map(data:Dictionary = {}) ->void:
	call_deferred("start_timer")
	var saved_map_location = data.save_map_location
	if(data!={}):
		if is_instance_valid(progress_bar_description):
			progress_bar_description.text = "Создание сохранения"
			progress_bar.set_value(30)
			await get_tree().create_timer(0.1).timeout
	
	var saved_map = FileAccess.open(saved_map_location, FileAccess.WRITE)
	if(data!={}):
		if is_instance_valid(progress_bar_description):
			progress_bar_description.text = "Сохранение в файл"
			progress_bar.set_value(60)
			await get_tree().create_timer(0.1).timeout
	
	var to_save_data = {
		"sites" = voronoi,
		"map_range" = ranges.map_range,
		"noise_data" = noise_data,
		"voronoi_lands_ids" = voronoi_lands_ids,
		"voronoi_inhabitable_lands_ids" = voronoi_inhabitable_lands_ids
	}
	var save_data:String = Delaunay_Bower.get_save_data_json(to_save_data)
	saved_map.store_line(save_data)
	saved_map.close()
	call_deferred("stop_timer")
	if(data!={}):
		progress_bar.set_value(100)
		if is_instance_valid(progress_bar_description):
			progress_bar_description.text = "Завершение"
			await get_tree().create_timer(0.1).timeout
	if(data!={}):
		call_deferred("voronoi_save_thread_done",data)


func save_civs_create_thread(_data:Dictionary = {}) ->void:
	var do_show_main_menu:bool = _data.do_show_main_menu if "do_show_main_menu" in _data else false
	var do_recalculate_civs:bool = _data.do_recalculate_civs if "do_recalculate_civs" in _data else false
	var save_civs_location:String = _data.save_civs_location if "save_civs_location" in _data else Files.get_fast_civ_location()
	restrict_all()
	loading_screen_thread_pool.append(Thread.new())
	civ_save_thread_pool.append(Thread.new())
	
	var loading_screen_thread_id = loading_screen_thread_pool.size()-1
	var loading_screen_thread:Thread = loading_screen_thread_pool[loading_screen_thread_id]
	var civ_save_thread_id = civ_save_thread_pool.size()-1
	var data = _data
	data.loading_screen_thread_id = loading_screen_thread_id
	data.civ_save_thread_id = civ_save_thread_id
	data.do_show_main_menu = do_show_main_menu
	data.save_civs_location = save_civs_location
	data.do_recalculate_civs = do_recalculate_civs
	
	var _loading_screen_thread_result = loading_screen_thread.start(Callable(self, "show_loading_screen_save_civs").bind(data))


func save_map_create_thread(_data:Dictionary = {}) ->void:
	var do_show_main_menu:bool = _data.do_show_main_menu if "do_show_main_menu" in _data else false
	var save_map_location:String = _data.save_map_location if "save_map_location" in _data else Files.get_fast_map_location()
	restrict_all()
	loading_screen_thread_pool.append(Thread.new())
	voronoi_save_thread_pool.append(Thread.new())
	
	var loading_screen_thread_id = loading_screen_thread_pool.size()-1
	var loading_screen_thread:Thread = loading_screen_thread_pool[loading_screen_thread_id]
	var data = {
		"do_show_main_menu" = do_show_main_menu,
		"save_map_location" = save_map_location,
		"loading_screen_thread_id" = loading_screen_thread_id,
		"voronoi_save_thread_id" = voronoi_save_thread_pool.size()-1
	}
	var _loading_screen_thread_result = loading_screen_thread.start(Callable(self, "show_loading_screen_save_map").bind(data))


func load_civ_create_thread(load_civ_location = Files.get_fast_civ_location()) ->void:
	restrict_all()
	loading_screen_thread_pool.append(Thread.new())
	civ_load_thread_pool.append(Thread.new())
	
	var loading_screen_thread_id = loading_screen_thread_pool.size()-1
	var loading_screen_thread:Thread = loading_screen_thread_pool[loading_screen_thread_id]
	var civ_load_thread_id = civ_load_thread_pool.size()-1
	var data:Dictionary = {
		"loading_screen_thread_id" = loading_screen_thread_id,
		"civ_load_thread_id" = civ_load_thread_id,
		"load_civ_location" = load_civ_location
	}
	var _loading_screen_thread_result = loading_screen_thread.start(Callable(self, "show_loading_screen_load_civ").bind(data))


func load_map_gen_civ_thread() ->void:
	var data = {
		"civ_has_to_be_created" = true
	}
	load_map_create_thread(data)


func load_map_civ_thread() ->void:
	var data = {
		"civ_has_to_be_loaded" = true
	}
	load_map_create_thread(data)


func load_map_create_thread(_data:Dictionary = {}) ->void:
	var civ_has_to_be_created:bool = _data.civ_has_to_be_created if "civ_has_to_be_created" in _data else false
	var civ_has_to_be_loaded:bool = _data.civ_has_to_be_loaded if "civ_has_to_be_loaded" in _data else false
	var load_map_location:String = _data.load_map_location if "load_map_location" in _data else Files.get_fast_map_location()
	
	restrict_all()
	loading_screen_thread_pool.append(Thread.new())
	voronoi_load_thread_pool.append(Thread.new())
	
	var loading_screen_thread_id = loading_screen_thread_pool.size()-1
	var loading_screen_thread:Thread = loading_screen_thread_pool[loading_screen_thread_id]
	var voronoi_load_thread_id = voronoi_load_thread_pool.size()-1
	var data:Dictionary = {
		"loading_screen_thread_id" = loading_screen_thread_id,
		"voronoi_load_thread_id" = voronoi_load_thread_id,
		"load_map_location" = load_map_location,
		"civ_has_to_be_loaded" = civ_has_to_be_loaded,
		"civ_has_to_be_created" = civ_has_to_be_created
	}
	var _loading_screen_thread_result = loading_screen_thread.start(Callable(self, "show_loading_screen_load_map").bind(data))


func load_civ(data:Dictionary) ->void:
	call_deferred("start_timer")
	var load_civ_location = data.load_civ_location
	var file = FileAccess.open(load_civ_location, FileAccess.READ)
	var json_file = JSON.new()
	json_file.parse(file.get_line())
	var civ_data = json_file.get_data()
	civs = {}
	ranges.civ_range = civ_data.civ_range
	
	if(civ_data.map_hash != Files.get_hash(Files.get_fast_map_location())):
		print("MAP HASHES ARE NOT EQUAL\nREGENERATING CIVS")
		data["force_generate_civ"] = true
		call_deferred("stop_timer")
		call_deferred("civ_load_thread_done",data)
		return
	
	spin_box.value = ranges.civ_range
#	spin_box.text = str(ranges.civ_range)
	for civ in civ_data.civs.values():
		var init_data:Dictionary = {
			"taken_sites_ids" = civ.taken_sites_ids,
			"chosen_name" = civ.civ_name,
			"chosen_color" = Color(civ.civ_color),
			"id" = civ.id
		}
		var new_civ:Civilization = Civilization.new(init_data)
		civs[new_civ["id"]] = new_civ
	if(generations.civ!=0):
		call_deferred("clear_civ_polygons",{"civ_site_generations":generations.civ})
	generations.civ+=1
	for civ in civs.values():
		call_deferred("show_civ_site",civ)
	
	call_deferred("stop_timer")
	call_deferred("civ_load_thread_done",data)


func load_map(data:Dictionary) ->void:
	call_deferred("start_timer")
	var load_map_location = data.load_map_location
	var file = FileAccess.open(load_map_location, FileAccess.READ)
	var json_file = JSON.new()
	json_file.parse(file.get_line())
	var map_data = json_file.get_data()
	voronoi = {}
	ranges.map_range = map_data.map_range
	
	voronoi_lands_ids = map_data.voronoi_lands_ids
	voronoi_inhabitable_lands_ids = map_data.voronoi_inhabitable_lands_ids
	
	var map_data_noise_data = map_data.noise_data
	var map_data_elevation = map_data_noise_data.elevation
	var map_data_moisture = map_data_noise_data.moisture
	
	noise_data = map_data_noise_data
	
	if(current_action in ([ACTION.GENERATE_MAP,ACTION.LOAD_MAP])):
#		line_edit_range.text = str(ranges.map_range)
		spin_box.value = ranges.map_range
		
		points_seed_slider.set_value(map_data_noise_data.points_seed)
		elevation_seed_slider.set_value(map_data_elevation.seed)
		moisture_seed_slider.set_value(map_data_moisture.seed)
		
		moisture_octaves_slider.set_value(map_data_moisture.fractal_octaves)
		moisture_lacunarity_slider.set_value(map_data_moisture.fractal_lacunarity)
		moisture_fractal_gain_slider.set_value(map_data_moisture.fractal_gain)
		moisture_frequency_slider.set_value(map_data_moisture.frequency)
		
		elevation_octaves_slider.set_value(map_data_elevation.fractal_octaves)
		elevation_lacunarity_slider.set_value(map_data_elevation.fractal_lacunarity)
		elevation_fractal_gain_slider.set_value(map_data_elevation.fractal_gain)
		elevation_frequency_slider.set_value(map_data_elevation.frequency)
	var sites_size = map_data.sites.keys().size()
	var sites_i = 0
	for site in map_data.sites.values():
		var new_site = Delaunay_Bower.VoronoiSite.new(Vector2(site.center.x,site.center.y))
		new_site.id = site.id
		new_site.animals = site.animals
		new_site.biome = site.biome
		new_site.drinking_water = site.drinking_water
		new_site.elevation = site.elevation
		new_site.fertility = site.fertility
		new_site.moisture = site.moisture
		new_site.is_land = site.is_land
		new_site.is_inhabitable_land = site.is_inhabitable_land
		new_site.province_name = site.province_name
		
		var minerals_dict = {}
		for type in site.minerals.keys():
			minerals_dict[int(type)] = {}
			for mineral in site.minerals[type]:
				minerals_dict[int(type)][int(mineral)] = float(site.minerals[type][mineral])
		new_site.minerals = minerals_dict

		var polygon_array:PackedVector2Array = []
		for poly in site.polygon:
			polygon_array.append(Vector2(poly.x,poly.y))
		new_site.polygon = polygon_array
		voronoi[new_site["id"]] = new_site
	
	if(generations.map!=0):
		if(data!={}):
			if is_instance_valid(progress_bar_description):
				progress_bar_description.text = "Удаляем старые полигоны"
				await get_tree().create_timer(0.1).timeout
		call_deferred("clear_map_polygons",{"map_site_generations":generations.map,"thread_status":THREAD_STATUS.LOAD})
	
	generations.map+=1
	await get_tree().create_timer(0.01).timeout
	for site in voronoi.values():
		if(data!={}):
			if(sites_i%100==0):
				if is_instance_valid(progress_bar_description):
					progress_bar_description.text = "Загрузка ячейки " + str(sites_i) + "/" + str(sites_size)
					var progress_bar_value:float = float((1.0/float(sites_size)) *80.0*100.0)
					progress_bar.set_value(float(progress_bar.get_value()) + progress_bar_value)
					await get_tree().create_timer(0.01).timeout
		sites_i+=1
		var saved_site = map_data["sites"].values()[site["id"]]
		var neighbours = []
		for neighbour in saved_site["neighbours"]:
			var edge = Delaunay_Bower.VoronoiEdge.new()
			edge.other = voronoi.values()[neighbour]
			edge.this = site
			neighbours.append(edge)
		site.neighbours = neighbours
		call_deferred("show_map_site",site)
	call_deferred("stop_timer")
	
	if(data!={}):
		call_deferred("voronoi_load_thread_done",data)


func show_civ_site(civ:Civilization):
	for site_id in civ.taken_sites_ids:
		var polygon:CivSite = civ_site_polygon_resource.instantiate()
		
		var data:Dictionary = {
			"layer_to_not_move_items" = non_movable_layer,
			"site" = voronoi.values()[site_id],
			"main_map" = self,
			"map_site_generations" = generations.map,
			"civ" = civ,
			"iteration" = generations.civ
		}
		polygon.init(data)
		add_child(polygon)


func show_map_site(site: Delaunay_Bower.VoronoiSite) ->void:
	var polygon:MapSite = map_site_polygon_resource.instantiate()
	var data:Dictionary = {
		"layer_to_not_move_items" = non_movable_layer,
		"site" = site,
		"main_map" = self,
		"restricted_zones" = restricted_zones,
		"map_site_generations" = generations.map
	}
	polygon.init(data)
	add_child(polygon)


func change_main_map_range(r) ->void:
	var _new_range = r
	if(_new_range > 1 && _new_range <= 10000):
		ranges.map_range = _new_range
		return
	if !(_new_range > 1):
		ranges.map_range = 2
		spin_box.value = 2
		return
	ranges.map_range = 10000
	spin_box.value = 10000
	return


func change_map_view()->void:
	pass


func change_civs_range(v) ->void:
	var _new_range = v
	var max_range = voronoi_inhabitable_lands_ids.size()
	if(_new_range > 1 && _new_range <= max_range):
		ranges.civ_range = _new_range
		return
	if !(_new_range > 1):
		ranges.civ_range = 2
		spin_box.value = 2
		return
	ranges.civ_range = max_range
	spin_box.value = max_range
	return


func generate_civ(ids:Dictionary) ->void:
	# Начальное время
	call_deferred("start_timer")
	
#	line_edit_range.text = str(ranges.civ_range)
	spin_box.value = ranges.civ_range
	
	voronoi_inhabitable_lands_chosen_ids = []
	civs_picked_colors = []
	civs_picked_names = []
	civs = {}
	
	while(voronoi_inhabitable_lands_chosen_ids.size() < ranges.civ_range):
		var choose = voronoi_inhabitable_lands_ids.pick_random()
		if not voronoi_inhabitable_lands_chosen_ids.has(choose):
			voronoi_inhabitable_lands_chosen_ids.append(choose)
	
	while(civs_picked_colors.size() < ranges.civ_range):
		var choose = Civilization.get_random_civ_color()
		if not civs_picked_colors.has(choose):
			civs_picked_colors.append(choose)
	while(civs_picked_names.size() < ranges.civ_range):
		var choose = Civilization.generate_civilization_name()
		if not civs_picked_names.has(choose):
			civs_picked_names.append(choose)
	
	site_to_civ.resize(voronoi.keys().size())
	for i in range(ranges.civ_range):
		var data:Dictionary = {
			"chosen_land" = int(voronoi_inhabitable_lands_chosen_ids[i]),
			"chosen_color" = civs_picked_colors[i],
			"chosen_name" = civs_picked_names[i],
			"id" = i
		}
		var civ:Civilization = Civilization.new(data)
		civs[i] = civ
		site_to_civ[int(voronoi_inhabitable_lands_chosen_ids[i])] = civ.id
	if(generations.civ!=0):
		call_deferred("clear_civ_polygons",{"civ_site_generations":generations.civ})
	generations.civ+=1
	for civ in civs.values():
		call_deferred("show_civ_site",civ)
	call_deferred("stop_timer")
	call_deferred("civ_gen_thread_done",ids)


func generate_voronoi_map(ids:Dictionary) ->void:
	call_deferred("start_timer") # Начальное время
#	line_edit_range.text = str(ranges.map_range)
	spin_box.value = ranges.map_range
	delone = Delaunay_Bower.new(Rect2(0,0,screen_size_x,screen_size_y))
	for _i in range(ranges.map_range):
		var point_added:bool = false
		while(!point_added):
			var new_point:Vector2 = Vector2(
				randf_range(0,screen_size_x),
				randf_range(0,screen_size_y))
			if not delone.points.has(new_point):
				delone.add_point(new_point)
				point_added = true
	delone.sort_points()
	progress_bar_description.text = "Генерация треугольников"
	var triangulars:Array = delone.triangulate(progress_bar)
	progress_bar_description.text = "Генерация ячеек Вороного"
	voronoi = delone.make_voronoi(triangulars,progress_bar)
	var e: Dictionary = get_elevation(voronoi) # Словарь с высотами (e[x][y] => высота)
	var m: Dictionary = get_moisture(voronoi) # Словарь с влажностями (m[x][y] => влажность)
	progress_bar_description.text = "Генерация биомов"
	var points_size = voronoi.size()
	for site in voronoi.values():
		var progress_bar_value = (1.0 / float(points_size * 3) * 100.0)
		progress_bar.set_value(float(progress_bar.get_value()) + float(progress_bar_value))
		var site_center = site.center
		var x = site_center.x
		var y = site_center.y
		biome_class.set_biome(site, e[x][y], m[x][y])
	progress_bar_description.text = "Удаление старых полигонов"
	if(generations.map!=0):
		call_deferred(
			"clear_map_polygons",
			{"map_site_generations":generations.map,"thread_status":THREAD_STATUS.CREATE})
	generations.map+=1
	for site in voronoi.values():
		biome_class.recalculate_shallow_biome_position(site)
		biome_class.set_biome_name(site)
		
	for i in range(3):
		for site in voronoi.values():
			biome_class.recalculate_shallow_biome_position(site)
	for site in voronoi.values():
		biome_class.make_shallow_between_near_lands(site)
	voronoi_lands_ids = []
	voronoi_inhabitable_lands_ids = []
	for site in voronoi.values():
		if(site.is_land):
			voronoi_lands_ids.append(site.id)
		if(site.is_inhabitable_land):
			voronoi_inhabitable_lands_ids.append(site.id)
		call_deferred("show_map_site",site)
	call_deferred("stop_timer")
	call_deferred("voronoi_gen_thread_done",ids)


func before_all() ->void:
	active_camera.set_offset(Vector2(screen_size_x/2,screen_size_y/2))
	
	genE.noise_type = FastNoiseLite.TYPE_PERLIN
	genE.fractal_type = FastNoiseLite.FRACTAL_FBM
	
	genM.noise_type = FastNoiseLite.TYPE_PERLIN
	genM.fractal_type = FastNoiseLite.FRACTAL_FBM
	
	notifications = load("res://Scenes/Notification/Notifications.tscn").instantiate()
	non_movable_layer.add_child(notifications)
	
	notifications_main_rect = notifications.get_node("Background")
	notifications_container = notifications_main_rect.get_node("ScrollContainer/VBoxContainer")
	
	are_visuals_hidden = false


func before_generate_civilizations() ->void:
	before_all()
	Files.check_user_civ_dir()
	
	visual_elements = load("res://Scenes/VisualElements/VisualElementsGenerateCivilizations.tscn").instantiate()
	visual_elements.init(self,loaded_map_location)
	non_movable_layer.add_child(visual_elements)
	
	set_restricted_zones()
	
	fast_save_button = visual_elements.get_node("ColorRect/SaveButton")
	fast_load_button = visual_elements.get_node("ColorRect/LoadButton")
#	line_edit_range = non_movable_layer.get_node("Visuals/ColorRect/LineEditRange")
	spin_box = non_movable_layer.get_node("Visuals/ColorRect/SpinBox")
	
	

func set_restricted_zones() ->void:
	var notifications_main_rect_position = notifications_main_rect.get_position()
	var notifications_main_rect_size = notifications_main_rect.get_size()
	
	notifications_main_rect_pool_vector = [
		Vector2(notifications_main_rect_position.x, notifications_main_rect_position.y),
		Vector2(notifications_main_rect_position.x, (notifications_main_rect_position + notifications_main_rect_size).y),
		Vector2((notifications_main_rect_position + notifications_main_rect_size).x,(notifications_main_rect_position + notifications_main_rect_size).y),
		Vector2((notifications_main_rect_position + notifications_main_rect_size).x,notifications_main_rect_position.y)
	]
	
	var visual_elements_main_rect:ColorRect = visual_elements.get_node("ColorRect")
	var visual_elements_main_rect_position = visual_elements_main_rect.get_position()
	var visual_elements_main_rect_size = visual_elements_main_rect.get_size()
	
	visual_elements_main_rect_pool_vector = [
		Vector2(visual_elements_main_rect_position.x, visual_elements_main_rect_position.y),
		Vector2(visual_elements_main_rect_position.x, (visual_elements_main_rect_position + visual_elements_main_rect_size).y),
		Vector2((visual_elements_main_rect_position + visual_elements_main_rect_size).x,(visual_elements_main_rect_position + visual_elements_main_rect_size).y),
		Vector2((visual_elements_main_rect_position + visual_elements_main_rect_size).x,visual_elements_main_rect_position.y)
	]
	
	restricted_zones = [notifications_main_rect_pool_vector,visual_elements_main_rect_pool_vector]


func before_generate_map() ->void:
	before_all()
#	Files.clear_maps_dir()
	delone = Delaunay_Bower.new(
		Rect2(
			0,
			0,
			screen_size_x,
			screen_size_y))
	Files.check_user_map_dir()
	
	visual_elements = load("res://Scenes/VisualElements/VisualElementsGenerateMap.tscn").instantiate()
	visual_elements.init(self)
	non_movable_layer.add_child(visual_elements)
	
	set_restricted_zones()
	
	var base_settings = visual_elements.get_node("ColorRect/BaseSettings")
	var map_settings = visual_elements.get_node("ColorRect/SelectableSettings/MapSettings")
	
	fast_save_button = base_settings.get_node("SaveButton")
	fast_load_button = base_settings.get_node("LoadButton")
	
	points_seed_slider = map_settings.get_node("PointsSeedContainer/PointsSeedSlider")
	elevation_seed_slider = map_settings.get_node("ElevationSeedContainer/ElevationSeedSlider")
	moisture_seed_slider = map_settings.get_node("MoistureSeedContainer/MoistureSeedSlider")
	
	elevation_octaves_slider = map_settings.get_node("ElevationOctavesContainer/ElevationOctavesSlider")
	elevation_frequency_slider = map_settings.get_node("ElevationFrequencyContainer/ElevationFrequencySlider")
	elevation_fractal_gain_slider = map_settings.get_node("ElevationFractalGainContainer/ElevationFractalGainSlider")
	elevation_lacunarity_slider = map_settings.get_node("ElevationLacunarityContainer/ElevationLacunaritySlider")
	elevation_lacunarity_slider.set_max(screen_size_x/300)
	
	moisture_octaves_slider = map_settings.get_node("MoistureOctaveContainer/MoistureOctaveSlider")
	moisture_frequency_slider = map_settings.get_node("MoistureFrequencyContainer/MoistureFrequencySlider")
	moisture_fractal_gain_slider = map_settings.get_node("MoistureFractalGainContainer/MoistureFractalGainSlider")
	moisture_lacunarity_slider = map_settings.get_node("MoistureLacunarityContainer/MoistureLacunaritySlider")
	moisture_lacunarity_slider.set_max(screen_size_x/400)
	
	spin_box = base_settings.get_node("SpinBox")
	
	
	if not FileAccess.file_exists(Files.get_fast_map_location()):
		fast_load_button.disabled = true


# Главная функция
func init(data:Dictionary) ->void:
	current_action = data.action
	fake_ready(data)


func fake_ready(data = null) ->void:
	match current_action:
		ACTION.LOAD_MAP:
			action_load_map(data)
		ACTION.GENERATE_MAP:
			action_generate_map()
		ACTION.GENERATE_CIVILIZATIONS:
			action_generate_civilizations()
		ACTION.GAME_OBSERVER:
			pass
		ACTION.GAME_PLAYER:
			pass


func action_load_map(data:Dictionary) ->void:
	before_generate_map()
	load_map_create_thread(data)


func action_generate_map() ->void:
	before_generate_map()
	if(Files.check_existing_file(Files.get_fast_map_location())):
		load_map_create_thread()
		return
	randomize_map_noise_data()
	generate_voronoi_map_create_thread()


func action_generate_civilizations() ->void:
	loaded_map_location = Files.get_fast_map_location()
	print(loaded_map_location)
	before_generate_civilizations()
	if(Files.check_existing_file(Files.get_fast_map_location())):
		if(Files.check_existing_file(Files.get_fast_civ_location())):
			var load_civ_location = Files.get_fast_civ_location()
			var file = FileAccess.open(load_civ_location, FileAccess.READ)
			var json_file = JSON.new()
			json_file.parse(file.get_line())
			var civ_data = json_file.get_data()
			if(civ_data.map_hash == Files.get_hash(Files.get_fast_map_location())):
				load_map_civ_thread()
				return
		load_map_gen_civ_thread()
		return
	show_main_menu()
	return


func generate_civs_create_thread() ->void:
	restrict_all()
	loading_screen_thread_pool.append(Thread.new())
	civ_gen_thread_pool.append(Thread.new())
	
	var loading_screen_thread_id = loading_screen_thread_pool.size()-1
	var loading_screen_thread:Thread = loading_screen_thread_pool[loading_screen_thread_id]
	var civ_gen_thread_id = civ_gen_thread_pool.size()-1
#	var ids = [loading_screen_thread_id,civ_gen_thread_id]
	var ids = {
		"loading_screen_thread_id" = loading_screen_thread_id,
		"civ_gen_thread_id" = civ_gen_thread_id
	}
	var _loading_screen_thread_result = loading_screen_thread.start(Callable(self, "show_loading_screen_generate_civ").bind(ids))


func generate_voronoi_map_create_thread() ->void:
	restrict_all()
	loading_screen_thread_pool.append(Thread.new())
	voronoi_gen_thread_pool.append(Thread.new())
	
	var loading_screen_thread_id = loading_screen_thread_pool.size()-1
	var loading_screen_thread:Thread = loading_screen_thread_pool[loading_screen_thread_id]
	var voronoi_gen_thread_id = voronoi_gen_thread_pool.size()-1
	var ids = {
		"loading_screen_thread_id" = loading_screen_thread_id,
		"voronoi_gen_thread_id" = voronoi_gen_thread_id
	}
	
	var _loading_screen_thread_result = loading_screen_thread.start(Callable(self, "show_loading_screen_generate_map").bind(ids))


func space_enter_action() ->void:
	if !is_space_enter_action_restricted:
		regenerate(current_action)


func hide_button_action() ->void:
	if !is_hide_button_action_restricted:
#		var visuals = get_tree().get_root().get_node("Main/MainMap/LayerToNotMoveItems/Visuals")
		var visuals = non_movable_layer.get_node("Visuals")
		if !are_visuals_hidden:
			visuals.hide()
			notifications_main_rect.hide()
			are_visuals_hidden = true
		else:
			visuals.show()
			notifications_main_rect.show()
			are_visuals_hidden = false


func continue_action_checker() ->void:
	if(closable_windows_array.size()==0):
		continue_action()
		return
	remove_all_freed_closable_windows()
	close_last_window()


func continue_action() ->void:
	var confirm_window:Node2D = load("res://Scenes/Common/Confirm.tscn").instantiate()
	non_movable_layer.add_child(confirm_window)
	closable_windows_array.append(confirm_window)
	var confirm_exit_window_color_rect:Confirm = confirm_window.get_node("ColorRect")
	confirm_exit_window_color_rect.init(self)
	var work_place = confirm_exit_window_color_rect.get_node("WorkPlace")
	var confirm_continue_placeholder:ConfirmContinue = load("res://Scenes/Common/ConfirmContinue.tscn").instantiate()
	var confirm_continue_placeholder_size:Vector2 = confirm_continue_placeholder.size
	work_place.add_child(confirm_continue_placeholder)
	confirm_continue_placeholder.init(self,confirm_exit_window_color_rect,current_action)
	confirm_exit_window_color_rect.size = Vector2(confirm_continue_placeholder_size.x,33+confirm_continue_placeholder_size.y)
	restrict_all()


func escape_action_checker() ->void:
	if(closable_windows_array.size()==0):
		escape_action()
		return
	remove_all_freed_closable_windows()
	close_last_window()


func escape_action() ->void:
	var confirm_window:Node2D = load("res://Scenes/Common/Confirm.tscn").instantiate()
	non_movable_layer.add_child(confirm_window)
	closable_windows_array.append(confirm_window)
	var confirm_exit_window_color_rect:Confirm = confirm_window.get_node("ColorRect")
	confirm_exit_window_color_rect.init(self)
	var work_place = confirm_exit_window_color_rect.get_node("WorkPlace")
	var confirm_exit_placeholder:VBoxContainer = load("res://Scenes/Common/ConfirmExit.tscn").instantiate()
	var confirm_exit_placeholder_size:Vector2 = confirm_exit_placeholder.size
	work_place.add_child(confirm_exit_placeholder)
	confirm_exit_placeholder.init(self)
	confirm_exit_window_color_rect.size = Vector2(confirm_exit_placeholder_size.x,33+confirm_exit_placeholder_size.y)
	restrict_all()


func mouse_wheel_down_action() ->void:
	var zoom = active_camera.get_zoom().x - 0.1
	if(zoom>0.01):
		active_camera.set_zoom(Vector2(zoom,zoom))


func mouse_wheel_up_action() ->void:
	var zoom = active_camera.get_zoom().x + 0.1
	active_camera.set_zoom(Vector2(zoom,zoom))


func remove_and_get_last_closable_window() ->Variant:
	var closable_windows_array_last = closable_windows_array.size()-1
	if(closable_windows_array_last!= -1):
		var wr = weakref(closable_windows_array[closable_windows_array_last])
		if !(wr.get_ref()):
			closable_windows_array.remove_at(closable_windows_array_last)
			return null
		return closable_windows_array.pop_back()
	else:
		return null


func remove_all_freed_closable_windows() ->void:
	for c_w in closable_windows_array:
		var wr:WeakRef = weakref(c_w)
		if (wr.get_ref()==null):
			closable_windows_array.erase(c_w)


func close_last_window() ->void:
	var closable_window = remove_and_get_last_closable_window()
	if closable_window && is_instance_valid(closable_window):
		closable_window.queue_free()
		if(closable_windows_array.size()==0):
			allow_all()


func _input(event:InputEvent) ->void:
	if event.is_action_released("space_enter"):
		space_enter_action()
		return
	if event.is_action_released("h_pressed"):
		hide_button_action()
		return
	if event.is_action_pressed("escape"):
		escape_action_checker()
		return
	if event.is_action_pressed("mouse_wheel_down"):
		if !is_scrolling_restricted:
			if !are_visuals_hidden:
				for zone in restricted_zones:
					if Geometry2D.is_point_in_polygon(event.position,zone):
						return
			mouse_wheel_down_action()
		return
	if event.is_action_pressed("mouse_wheel_up"):
		if !is_scrolling_restricted:
			if !are_visuals_hidden:
				for zone in restricted_zones:
					if Geometry2D.is_point_in_polygon(event.position,zone):
						return
			mouse_wheel_up_action()
			return
	if(!is_moving_map_restricted):
		if event.is_action_pressed("keyboard_down"):
			move_camera_thread(DIRECTION.DOWN)
			return
		if event.is_action_released("keyboard_down"):
			move_camera_thread(DIRECTION.DOWN,false)
			return
		if event.is_action_pressed("keyboard_left"):
			move_camera_thread(DIRECTION.LEFT)
			return
		if event.is_action_released("keyboard_left"):
			move_camera_thread(DIRECTION.LEFT,false)
			return
		if event.is_action_pressed("keyboard_right"):
			move_camera_thread(DIRECTION.RIGHT)
			return
		if event.is_action_released("keyboard_right"):
			move_camera_thread(DIRECTION.RIGHT,false)
			return
		if event.is_action_pressed("keyboard_up"):
			move_camera_thread(DIRECTION.TOP)
			return
		if event.is_action_released("keyboard_up"):
			move_camera_thread(DIRECTION.TOP,false)
			return
	

func find_point_id_by_position(point_position: Vector2) ->int:
	var min_distance:float = screen_size_x
	var min_id:int = 0
	for site in voronoi.values():
		var distance = site.center.distance_to(point_position)
		if(distance<min_distance):
			min_distance = distance
			min_id = site.id
	return min_id


func show_main_menu() ->void:
	for child in self.get_children():
		if(is_instance_valid(child)):
			child.queue_free()
	if(is_instance_valid(self)):
		queue_free()
	get_tree().change_scene_to_packed(main_packed)
	


func show_main_menu_save_map() ->void:
	var data:Dictionary = {
		"do_show_main_menu" = true
	}
	save_map_create_thread(data)


func show_main_menu_save_civilizations() ->void:
	var data:Dictionary = {
		"do_show_main_menu" = true
	}
	save_civs_create_thread(data)


func create_save_as_window(action:ACTION) ->void:
	if(is_save_as_window_active):
		return
	var save_as_window = load("res://Scenes/Common/SaveAs.tscn").instantiate()
	non_movable_layer.add_child(save_as_window)
	closable_windows_array.append(save_as_window)
	var save_as_window_rect:SaveAs = save_as_window.get_node("ColorRect")
	save_as_window_rect.init(action,self)
	restrict_all()


func create_load_as_window(action:ACTION) ->void:
	if(is_load_as_window_active):
		return
	var load_as_window:LoadAs = load("res://Scenes/Common/LoadAs.tscn").instantiate()
	non_movable_layer.add_child(load_as_window)
	closable_windows_array.append(load_as_window)
	load_as_window.init({'action':action})
	var item_list:ItemList = load_as_window.get_node("ColorRect/WorkPlace/ItemList")
	var save_locations
	match action:
		ACTION.LOAD_CIVILIZATIONS:
			save_locations = Files.get_civilizations_saves_by_map(loaded_map_location)
		ACTION.LOAD_MAP:
			save_locations = Files.get_map_saves()
	for save_location in save_locations:
		item_list.add_item(save_location)
	restrict_all()


func add_notification(text:String) ->Label:
	var notification_label:Label = load("res://Scenes/Notification/NotificationLabel.tscn").instantiate()
	notification_label.text = text
	notifications_container.add_child(notification_label)
	notifications_container.move_child(notification_label, 0)
	return notification_label


func add_notification_error(text:String) ->void:
	var notification_label:Label = add_notification(text)
	var new_sb = StyleBoxFlat.new()
	new_sb.bg_color = Color(1,0,0,0.4)
	notification_label.add_theme_stylebox_override("normal", new_sb)


func add_notification_ok(text:String) ->void:
	var notification_label:Label = add_notification(text)
	var new_sb = StyleBoxFlat.new()
	new_sb.bg_color = Color(0,1,0,0.4)
	notification_label.add_theme_stylebox_override("normal", new_sb)


func add_notification_alarm(text:String) ->void:
	var notification_label:Label = add_notification(text)
	var new_sb = StyleBoxFlat.new()
	new_sb.bg_color = Color(1,1,0,0.4)
	notification_label.add_theme_stylebox_override("normal", new_sb)


func civ_gen_thread_done(ids:Dictionary) ->void:
	var loading_screen_thread_id = ids.loading_screen_thread_id
	var civ_gen_thread_id = ids.civ_gen_thread_id
	
	while(civ_gen_thread_pool[civ_gen_thread_id].is_alive()):
		pass
	civ_gen_thread_pool[civ_gen_thread_id].wait_to_finish()
	civ_gen_thread_pool[civ_gen_thread_id] = null
	while(loading_screen_thread_pool[loading_screen_thread_id].is_alive()):
		pass
	loading_screen_thread_pool[loading_screen_thread_id].wait_to_finish()
	loading_screen_thread_pool[loading_screen_thread_id] = null
	call_deferred("allow_all")
	if is_instance_valid(loading_screen):
		loading_screen.queue_free()
	call_deferred("add_notification_ok","Создано новых цивилизаций: " + str(ranges.civ_range))


func voronoi_gen_thread_done(ids:Dictionary) ->void:
	var loading_screen_thread_id = ids.loading_screen_thread_id
	var voronoi_gen_thread_id = ids.voronoi_gen_thread_id
	
	while(voronoi_gen_thread_pool[voronoi_gen_thread_id].is_alive()):
		pass
	voronoi_gen_thread_pool[voronoi_gen_thread_id].wait_to_finish()
	voronoi_gen_thread_pool[voronoi_gen_thread_id] = null
	while(loading_screen_thread_pool[loading_screen_thread_id].is_alive()):
		pass
	loading_screen_thread_pool[loading_screen_thread_id].wait_to_finish()
	loading_screen_thread_pool[loading_screen_thread_id] = null
	call_deferred("allow_all")
	if is_instance_valid(loading_screen):
		loading_screen.queue_free()
	call_deferred("add_notification_ok","Создана новая карта с кол-вом точек " + str(ranges.map_range))
#	add_notification_ok("Создана новая карта с кол-вом точек " + str(ranges.map_range))
#	get_tree().reload_current_scene()


func civ_load_thread_done(data:Dictionary) ->void:
	var loading_screen_thread_id = data.loading_screen_thread_id
	var civ_load_thread_id = data.civ_load_thread_id
	while(civ_load_thread_pool[civ_load_thread_id].is_alive()):
		pass
	civ_load_thread_pool[civ_load_thread_id].wait_to_finish()
	civ_load_thread_pool[civ_load_thread_id] = null
	while(loading_screen_thread_pool[loading_screen_thread_id].is_alive()):
		pass
	loading_screen_thread_pool[loading_screen_thread_id].wait_to_finish()
	loading_screen_thread_pool[loading_screen_thread_id] = null
	if(is_instance_valid(loading_screen)):
		loading_screen.queue_free()
	
	if("force_generate_civ" in data):
		current_action = ACTION.GENERATE_CIVILIZATIONS
		call_deferred('action_generate_civilizations')
		return
	
	call_deferred("allow_all")
	call_deferred("add_notification_ok","Цивилизация загружена из пути " + data.load_civ_location)


func voronoi_load_thread_done(data:Dictionary) ->void:
	var loading_screen_thread_id = data.loading_screen_thread_id
	var voronoi_load_thread_id = data.voronoi_load_thread_id
	while(voronoi_load_thread_pool[voronoi_load_thread_id].is_alive()):
		pass
	voronoi_load_thread_pool[voronoi_load_thread_id].wait_to_finish()
	voronoi_load_thread_pool[voronoi_load_thread_id] = null
	while(loading_screen_thread_pool[loading_screen_thread_id].is_alive()):
		pass
	loading_screen_thread_pool[loading_screen_thread_id].wait_to_finish()
	loading_screen_thread_pool[loading_screen_thread_id] = null
	call_deferred("allow_all")
	if(is_instance_valid(loading_screen)):
		loading_screen.queue_free()
#	if(current_action in ([ACTION.GENERATE_MAP,ACTION.LOAD_MAP,ACTION.GENERATE_CIVILIZATIONS,ACTION.LOAD_CIVILIZATIONS])):
	call_deferred("add_notification_ok","Карта загружена из пути " + data.load_map_location)
#	add_notification_ok("Карта загружена из пути " + data.load_map_location)
	if(data.civ_has_to_be_loaded):
		call_deferred("load_civ_create_thread")
#		load_civ_create_thread()
	elif(data.civ_has_to_be_created):
		call_deferred("randomize_civ_data")
		call_deferred("generate_civs_create_thread")


func civs_save_thread_done(data:Dictionary) ->void:
	var loading_screen_thread_id = data.loading_screen_thread_id
	var civ_save_thread_id = data.civ_save_thread_id
	while(civ_save_thread_pool[civ_save_thread_id].is_alive()):
		pass
	civ_save_thread_pool[civ_save_thread_id].wait_to_finish()
	civ_save_thread_pool[civ_save_thread_id] = null
	
	while(loading_screen_thread_pool[loading_screen_thread_id].is_alive()):
		pass
	loading_screen_thread_pool[loading_screen_thread_id].wait_to_finish()
	loading_screen_thread_pool[loading_screen_thread_id] = null
	if(is_instance_valid(loading_screen)):
		loading_screen.queue_free()
	call_deferred("add_notification_ok","Цивилизация сохранена по пути " + data.save_civs_location)
	call_deferred("allow_all")
	if(data.do_show_main_menu):
		show_main_menu()
	if(data.do_recalculate_civs):
		print('recalculate_civs_locations')
		visual_elements.recalculate_civs_locations()


func voronoi_save_thread_done(data:Dictionary) ->void:
	var loading_screen_thread_id = data.loading_screen_thread_id
	var voronoi_save_thread_id = data.voronoi_save_thread_id
	while(voronoi_save_thread_pool[voronoi_save_thread_id].is_alive()):
		pass
	voronoi_save_thread_pool[voronoi_save_thread_id].wait_to_finish()
	voronoi_save_thread_pool[voronoi_save_thread_id] = null
	while(loading_screen_thread_pool[loading_screen_thread_id].is_alive()):
		pass
	loading_screen_thread_pool[loading_screen_thread_id].wait_to_finish()
	loading_screen_thread_pool[loading_screen_thread_id] = null
	if(is_instance_valid(loading_screen)):
		loading_screen.queue_free()
	call_deferred("add_notification_ok","Карта сохранена по пути " + data.save_map_location)
	call_deferred("allow_all")
	if(data.do_show_main_menu):
		show_main_menu()
	


func show_civ_info(civ_id:int):
	print("show civ " + str(civ_id) + " info")


func allow_moving_map() ->void:
	is_moving_map_restricted = false


func restrict_moving_map() ->void:
	is_moving_map_restricted = true


func restrict_scrolling() ->void:
	is_scrolling_restricted = true


func allow_scrolling() ->void:
	is_scrolling_restricted = false


func restrict_hide_button_action() ->void:
	is_hide_button_action_restricted = true


func allow_hide_button_action() ->void:
	is_hide_button_action_restricted = false


func set_load_as_window_inactive() ->void:
	is_load_as_window_active = false


func set_load_as_window_active() ->void:
	is_load_as_window_active = true


func set_save_as_window_inactive() ->void:
	is_save_as_window_active = false


func set_save_as_window_active() ->void:
	is_save_as_window_active = true


func restrict_space_enter_action() ->void:
	is_space_enter_action_restricted = true


func allow_space_enter_action() ->void:
	is_space_enter_action_restricted = false


func restrict_selecting_site() ->void:
	is_selecting_site_restricted = true


func allow_selecting_site() ->void:
	is_selecting_site_restricted = false


func restrict_all() ->void:
	restrict_scrolling()
	restrict_hide_button_action()
	restrict_space_enter_action()
	restrict_selecting_site()
	disable_polygons_highlight()
	restrict_moving_map()


func allow_all() ->void:
	allow_scrolling()
	allow_hide_button_action()
	allow_space_enter_action()
	allow_selecting_site()
	enable_polygons_highlight()
	allow_moving_map()
