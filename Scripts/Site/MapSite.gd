class_name MapSite
extends Polygon2D


var is_line_shown:bool = false
var is_showing_line_restricted:bool = false
var is_showing_line_restricted_forever:bool
var main_map:MainMapGenerator
var restricted_zones
var iteration:int
var line_points:PackedVector2Array
const LINE_WIDTH:int = 3
var line_default_color:Color
const RESTRICTED_BIOMES:Array = [Biome.get.OCEAN, Biome.get.SHALLOW]
var voronoi_site: Delaunay_Bower.VoronoiSite
var layer_to_not_move_items
var line:Line2D = Line2D.new()
var sorted_x_polygon:PackedVector2Array
var sorted_y_polygon:PackedVector2Array
var polygon_length:float
var polygon_height:float
var center:Vector2
var label_font:FontFile = preload("res://Fonts/Adventure.ttf")


func restrict_showing_polygon_line() ->void:
	is_showing_line_restricted = true


func allow_showing_polygon_line() ->void:
	is_showing_line_restricted = false


func init(data:Dictionary) ->void:
	layer_to_not_move_items = data.layer_to_not_move_items
	voronoi_site = data.site
	set_polygon(voronoi_site.polygon)
	color = Biome.get_color(voronoi_site.biome)
	main_map = data.main_map
	restricted_zones = data.restricted_zones
	iteration = data.map_site_generations
	if(voronoi_site.biome in (RESTRICTED_BIOMES)):
		is_showing_line_restricted_forever = true
	line_points = polygon
	line_points.append(line_points[0])
	if(voronoi_site.biome in ([Biome.get.SNOW])):
		line_default_color = Color.RED
	else:
		line_default_color = Color.WHITE
	
	sorted_x_polygon = polygon.duplicate()
	PV2ArrayMethods.sort_by_x(sorted_x_polygon)

	sorted_y_polygon = polygon.duplicate()
	PV2ArrayMethods.sort_by_y(sorted_y_polygon)

	polygon_length = sorted_x_polygon[-1].x - sorted_x_polygon[0].x
	polygon_height = sorted_y_polygon[-1].y - sorted_y_polygon[0].y
	
	center = Vector2(
		sorted_x_polygon[0].x + polygon_length/2,
		sorted_y_polygon[0].y + polygon_height/2,
		)
	
	init_line()



func init_line() ->void:
	line.points = line_points
	line.width = LINE_WIDTH
	line.default_color = line_default_color
	line.set_antialiased(false)
	add_child(line)
	line.visible = false


func _on_node_2d_draw():
	if !is_showing_line_restricted_forever:
		var l:Label = Label.new()
		l.text = voronoi_site.province_name
		l.add_theme_font_override('adventure',label_font)
		l.add_theme_font_size_override('size',16)
		l.position = center
		l.visible = false
		$Node2D.draw_string(
			label_font,
			Vector2(
				center.x - l.size.x/2,
				center.y
				),
			voronoi_site.province_name,
			HORIZONTAL_ALIGNMENT_CENTER,
			-1,
			16,
			Color.BLACK,
			TextServer.JUSTIFICATION_NONE,
			TextServer.DIRECTION_AUTO,
			TextServer.ORIENTATION_HORIZONTAL
			)
		l.free()
		move_child($Node2D,0)
		if(!line.visible):
			$Node2D.visible = false


func show_polygon_line() ->void:
	if !is_showing_line_restricted && !is_line_shown:
		line.visible = true
		$Node2D.visible = true
		is_line_shown = true


func remove_polygon_line() ->void:
	if(is_line_shown):
		line.visible = false
		$Node2D.visible = false
		is_line_shown = false


func _input(event:InputEvent) ->void:
	if("position" in event):
		highlight_polygon_line(event)
		if !main_map.is_selecting_site_restricted && event.is_action_pressed("right_mouse") && (Geometry2D.is_point_in_polygon(get_global_mouse_position(),polygon)):
			right_mouse_action()
			return


func right_mouse_action() ->void:
	select_site()


func select_current_site(site:Delaunay_Bower.VoronoiSite = null) ->void:
	var used_site:Delaunay_Bower.VoronoiSite = null
	if(site==null):
		used_site = voronoi_site
	else:
		used_site = site
	var biome:int = used_site.biome
	var elevation:float = snapped(used_site.elevation*100,0.01)
	var moisture:float = snapped(used_site.moisture*100,0.01)
	var fertility:float = snapped(used_site.fertility*100,0.01)
	var drinking_water:float = snapped(used_site.drinking_water*100,0.01)
	var animals:float = snapped(used_site.animals*100,0.01)
	var biome_str:String = Biome.get_name_ru(biome)
	var minerals:Dictionary = used_site.minerals
	var minerals_str:String = ""
	var is_land:bool = used_site.is_land
	var is_inhabitable_land:bool = used_site.is_inhabitable_land
	
	for type_id in minerals.keys():
		var type_data = minerals.values()[type_id]
		if(type_data.keys().size()==0):
			continue
		for mineral in type_data.keys():
			minerals_str += Mineral.get_name_ru(type_id,mineral) + ", "
	minerals_str = minerals_str.left(-2)
	
	var edit_site_window = load("res://Scenes/Common/EditSite.tscn").instantiate()
	layer_to_not_move_items.add_child(edit_site_window)
	main_map.closable_windows_array.append(edit_site_window)
	var edit_site_color_rect:EditSite = edit_site_window.get_node("Main")
	edit_site_color_rect.init(main_map)
	
	var vbox_container:VBoxContainer = edit_site_window.get_node("Main/Place_For_Labels/ScrollContainer/VBoxContainer")
	var id_label:Label = vbox_container.get_node("ID_Label")
	var biome_label:Label = vbox_container.get_node("Biome_Label")
	var elevation_label:Label = vbox_container.get_node("Elevation_Label")
	var moisture_label:Label = vbox_container.get_node("Moisture_Label")
	var fertility_label:Label = vbox_container.get_node("Fertility_Label")
	var dw_label: Label = vbox_container.get_node("DW_Label")
	var animals_label: Label = vbox_container.get_node("Animals_Label")
	var header_label: Label = edit_site_window.get_node("Main/Header/Header_Label")
	var neighbours_container: HFlowContainer = vbox_container.get_node("Neighbours_Container")
	var civ_container:HFlowContainer = vbox_container.get_node("Civ_Container")
	var neighbours_label: Label = neighbours_container.get_node("Neighbours_Label")
	var minerals_label: Label = vbox_container.get_node("Minerals_Label")
	var is_land_label: Label = vbox_container.get_node("IsLandLabel")
	var is_inh_label: Label = vbox_container.get_node("IsInhabitableLandLabel")
	
	is_land_label.text = "Земля" if is_land else "Не земля"
	is_inh_label.text = "Населяемая земля" if is_inhabitable_land else "Ненаселяемая земля"
	id_label.text = "ID: " + str(used_site.id)
	biome_label.text = "Биом: " + biome_str
	elevation_label.text = "Высота: " + str(elevation) + "%"
	moisture_label.text = "Влажность: " + str(moisture) + "%"
	fertility_label.text = "Плодородность: " + str(fertility) + "%"
	dw_label.text = "Питьевая вода: " + str(drinking_water) + "%"
	animals_label.text = "Животные: " + str(animals) + "%"
	header_label.text = used_site.province_name
	neighbours_label.text = "ID соседних ячеек: "
	if(minerals_str == ""):
		minerals_label.text = "Полезные ископаемые отсутствуют"
	else:
		minerals_label.text = "Полезные ископаемые: " + minerals_str
	
	var last_neighbour = used_site.neighbours[-1].other.id
	for neighbour_egde in used_site.neighbours: 
		# neighbour_egde является объектом подкласса VoronoiEdge класса Delaunay_Bower
		var neighbour_id_label:NeighbourLabel = load("res://Scenes/Label/NeighbourLabel.tscn").instantiate()
		neighbours_container.add_child(neighbour_id_label)
		neighbour_id_label.init(self)
		neighbour_id_label.id = neighbour_egde.other.id
		neighbour_id_label.text = str(neighbour_egde.other.id)
		var separator_label:Label = load("res://Scenes/Label/SeparatorLabel.tscn").instantiate()
		if(last_neighbour!=neighbour_egde.other.id):
			neighbours_container.add_child(separator_label)
			separator_label.text = ", "
	
	if(main_map.site_to_civ.size()!=0):
		if(main_map.site_to_civ[used_site.id]!=null):
			var civ_label:CivLabel = load("res://Scenes/Label/CivLabel.tscn").instantiate()
			var civ:Civilization = main_map.civs.values()[main_map.site_to_civ[used_site.id]]
			civ_container.add_child(civ_label)
			civ_label.init(main_map)
			civ_label.id = civ.id
			civ_label.text = civ.civ_name


func select_site(site_id:int = -1) ->void:
	if(site_id==-1):
		select_current_site()
		return
	var site = main_map.voronoi[site_id]
	select_current_site(site)
	

func highlight_polygon_line(event) ->void:
	if(is_instance_valid(self)):
		if !is_showing_line_restricted_forever && (Geometry2D.is_point_in_polygon(get_global_mouse_position(),polygon)):
			if !main_map.are_visuals_hidden:
				for zone in main_map.restricted_zones:
					if !(Geometry2D.is_point_in_polygon(event.position,zone)):
						pass
					else:
						remove_polygon_line()
						return
			show_polygon_line()
			return
		remove_polygon_line()



