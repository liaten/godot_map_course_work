class_name MainMenu
extends ColorRect

var screen_size_x: float = DisplayServer.screen_get_size().x
var screen_size_y: float = DisplayServer.screen_get_size().y
var is_load_as_window_active: bool = false
var is_map_generated: bool = false

var load_as_window_resource:Resource = preload("res://Scenes/Common/LoadAs.tscn")
var load_civ_as_window_resource:Resource = preload("res://Scenes/Common/LoadCivAs.tscn")
var generate_map_packed:PackedScene = ResourceLoader.load("res://Scenes/Map/GenerateMapScene.tscn")


func _on_Exit_Button_pressed() ->void:
	get_tree().quit()


func _on_Generate_Map_Button_pressed() ->void:
	var main_game_node:Node2D = get_tree().get_root().get_node("Main")
	for child in self.get_children():
		if(is_instance_valid(child)):
			child.queue_free()
	
	get_tree().change_scene_to_packed(generate_map_packed)
	main_game_node.queue_free()


func _on_Load_Map_Button_pressed() ->void:
	if(!is_load_as_window_active):
		create_load_as_window(MainMapGenerator.ACTION.LOAD_MAP)
		set_load_as_window_active()


func create_load_as_window(action: MainMapGenerator.ACTION) ->void:
	var data = {}
	data.main_menu = self
	match action:
		MainMapGenerator.ACTION.LOAD_MAP:
			var load_as_window:LoadAs = load_as_window_resource.instantiate()
			data.action = action
			load_as_window.init(data)
			get_tree().get_root().get_node("Main").add_child(load_as_window)
			var load_as_color_rect = load_as_window.get_node("ColorRect")
			load_as_color_rect.position = Vector2(screen_size_x/2 - load_as_color_rect.size.x/2, screen_size_y/2 - load_as_color_rect.size.y/2)
			var item_list:ItemList = load_as_window.get_node("ColorRect/WorkPlace/ItemList")
			for map in Files.get_map_saves():
				item_list.add_item(map)
		MainMapGenerator.ACTION.LOAD_CIVILIZATIONS:
			var load_as_window:LoadCivAs = load_civ_as_window_resource.instantiate()
			load_as_window.init(data)
			get_tree().get_root().get_node("Main").add_child(load_as_window)
			var load_as_color_rect = load_as_window.get_node("ColorRect")
			load_as_color_rect.position = Vector2(screen_size_x/2 - load_as_color_rect.size.x/2, screen_size_y/2 - load_as_color_rect.size.y/2)
			var item_list:ItemList = load_as_window.get_node("ColorRect/WorkPlace/Lists/MapList")
			for map in Files.get_map_saves():
				item_list.add_item(map)
			for civ in Files.get_civilizations_saves():
				var file = FileAccess.open(Files.civ_folder + civ, FileAccess.READ)
				var json_file = JSON.new()
				json_file.parse(file.get_line())
				var civ_data = json_file.get_data()
				load_as_window.init_civ_hash(civ_data.map_hash,civ)


func set_load_as_window_inactive() ->void:
	is_load_as_window_active = false


func set_load_as_window_active() ->void:
	is_load_as_window_active = true


func _on_generate_map_check_box_toggled(_button_pressed) ->void:
	is_map_generated = true
	$Generate_Civilizations_Button.set_disabled(false)


func _on_generate_civilizations_button_pressed() ->void:
	var main_map_generator:MainMapGenerator = load("res://Scenes/Map/MainMapGenerator.tscn").instantiate()
	var main_game_node:Node2D = get_tree().get_root().get_node("Main")
	for child in self.get_children():
		if(is_instance_valid(child)):
			child.queue_free()
	main_game_node.add_child(main_map_generator)
	var data: Dictionary = {
		"action" = MainMapGenerator.ACTION.GENERATE_CIVILIZATIONS
	}
	main_map_generator.init(data)
	if(is_instance_valid(self)):
		self.queue_free()


func _on_load_civilizations_button_pressed():
	if(!is_load_as_window_active):
		create_load_as_window(MainMapGenerator.ACTION.LOAD_CIVILIZATIONS)
		set_load_as_window_active()
