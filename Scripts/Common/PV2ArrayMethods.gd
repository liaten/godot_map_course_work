class_name PV2ArrayMethods
const MAX_FLOAT_VALUE = 999_999_999.0

static func sort_by_x(arr:PackedVector2Array):
	arr.sort()


static func sort_by_y(arr:PackedVector2Array):
	# Bubble sort
	for i in range(arr.size()-1):
		for j in range(arr.size()-i-1):
			if arr[j].y >arr[j+1].y:
				var temp = arr[j]
				arr[j] = arr[j+1]
				arr[j+1] = temp
