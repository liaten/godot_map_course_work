class_name Biome


var existing_provinces_names: Array = []

enum get{
	SHALLOW,
	HIGHLANDS,
	SANDY_BEACH,
	GRASSLAND,
	OCEAN,
	SAVANNAH,
	SCORCHED,
	SHRUBLAND,
	SNOW,
	SWAMP,
	TEMPERATE_RAIN_FOREST,
	TROPICAL_RAIN_FOREST,
	TROPICAL_SEASONAL_FOREST,
	HALF_DESERT,
	DESERT
}


static func get_name_ru(biome:int) -> String:
	match biome:
		get.SHALLOW: # Мелководье
			return "Мелководье"
		get.HIGHLANDS: # Возвышенность
			return "Возвышенность"
		get.SANDY_BEACH: # Песчаный пляж
			return "Песчаный пляж"
		get.DESERT: # Пустыня
			return "Пустыня"
		get.HALF_DESERT:
			return "Полупустыня"
		get.GRASSLAND: # Трава
			return "Трава"
		get.OCEAN: # Океан
			return "Океан"
		get.SAVANNAH: # Саванна
			return "Саванна"
		get.SCORCHED: # Скала
			return "Скала"
		get.SNOW: # Горный снег
			return "Горный снег"
		get.SWAMP: # Болото
			return "Болото"
		get.TEMPERATE_RAIN_FOREST:
			return "Умеренный дождевой лес"
		get.TROPICAL_RAIN_FOREST:
			return "Тропический дождевой лес"
		get.TROPICAL_SEASONAL_FOREST:
			return "Тропический сезонный лес"
	return "ОШИБКА"


static func get_color(biome:int) -> Color:
	match biome:
		get.SHALLOW: # Мелководье
			return Color("#111664")
		get.HIGHLANDS: # Возвышенность
			return Color("#3d2f0d")
		get.SANDY_BEACH: # Песчаный пляж
			return Color("#ffd561")
		get.DESERT: # Пустыня
			return Color("#ffd561")
		get.HALF_DESERT:
			return Color("#efff61")
		get.GRASSLAND: # Трава
			return Color("#5b8a03")
		get.OCEAN: # Океан
			return Color("#0B0E3F")
		get.SAVANNAH: # Саванна
			return Color("#a4e607")
		get.SCORCHED: # Скала
			return Color("#636363")
		get.SNOW: # Горный снег
			return Color(1,1,1,1)
		get.SWAMP: # Болото
			return Color("#1d2b03")
		get.TEMPERATE_RAIN_FOREST: # Умеренный дождевой лес
			return Color("#3e5e03")
		get.TROPICAL_RAIN_FOREST: # Тропический дождевой лес
			return Color("#253803")
		get.TROPICAL_SEASONAL_FOREST: # Тропический сезонный лес
			return Color("#2f4703")
	return Color.BLACK


static func calculate_fertility(biome:int) -> float:
	match biome:
		get.HIGHLANDS: # Возвышенность
			return randf_range(0.1,0.15)
		get.SANDY_BEACH: # Песчаный пляж
			return randf_range(0.1,0.15)
		get.DESERT: # Пустыня
			return randf_range(0.0,0.05)
		get.HALF_DESERT:
			return randf_range(0.1,0.15)
		get.GRASSLAND: # Трава
			return randf_range(0.8,1.0)
		get.SAVANNAH: # Саванна
			return randf_range(0.3,0.6)
		get.SWAMP: # Болото
			return randf_range(0.3,1.0)
		get.TEMPERATE_RAIN_FOREST: # Умеренный дождевой лес
			return randf_range(0.3,0.5)
		get.TROPICAL_RAIN_FOREST: # Тропический дождевой лес
			return randf_range(0.5,0.8)
		get.TROPICAL_SEASONAL_FOREST: # Тропический сезонный лес
			return randf_range(0.9,1.0)
	return 0.0


static func calculate_drinking_water(biome:int) -> float:
	match biome:
		get.HIGHLANDS: # Возвышенность
			return randf_range(0.3,0.8)
		get.SANDY_BEACH: # Песчаный пляж
			return randf_range(0.1,0.15)
		get.DESERT: # Пустыня
			return randf_range(0.0,0.01)
		get.HALF_DESERT:
			return randf_range(0.1,0.15)
		get.GRASSLAND: # Трава
			return randf_range(0.4,1.0)
		get.SAVANNAH: # Саванна
			return randf_range(0.4,0.9)
		get.SWAMP: # Болото
			return randf_range(0.1,0.3)
		get.TEMPERATE_RAIN_FOREST: # Умеренный дождевой лес
			return randf_range(0.5,0.8)
		get.TROPICAL_RAIN_FOREST: # Тропический дождевой лес
			return randf_range(0.8,1.0)
		get.TROPICAL_SEASONAL_FOREST: # Тропический сезонный лес
			return randf_range(0.9,1.0)
		get.SNOW: # Горный снег
			return randf_range(0.9,1.0)
	return 0.0

static func calculate_animals(biome:int) -> float:
	match biome:
		get.SHALLOW: # Мелководье
			return randf_range(0.4,0.6)
		get.HIGHLANDS: # Возвышенность
			return randf_range(0.1,0.6)
		get.SANDY_BEACH: # Песчаный пляж
			return randf_range(0.1,0.3)
		get.DESERT: # Пустыня
			return randf_range(0.1,0.2)
		get.HALF_DESERT:
			return randf_range(0.2,0.3)
		get.GRASSLAND: # Трава
			return randf_range(0.2,1.0)
		get.OCEAN: # Океан
			return randf_range(0.3,0.9)
		get.SAVANNAH: # Саванна
			return randf_range(0.6,1.0)
		get.SCORCHED: # Скала
			return randf_range(0.1,0.3)
		get.SNOW: # Горный снег
			return randf_range(0.0,0.1)
		get.SWAMP: # Болото
			return randf_range(0.1,0.8)
		get.TEMPERATE_RAIN_FOREST: # Умеренный дождевой лес
			return randf_range(0.4,1.0)
		get.TROPICAL_RAIN_FOREST: # Тропический дождевой лес
			return randf_range(0.6,1.0)
		get.TROPICAL_SEASONAL_FOREST: # Тропический сезонный лес
			return randf_range(0.8,1.0)
	return 0.0


func calculate_minerals(site) ->Dictionary:
	var minerals: Dictionary = {}
	for type in Mineral.type.values():
		minerals[type] = {}
		
	if (site.biome in ([get.OCEAN,get.SHALLOW])):
		return minerals
	
	if(site.biome in ([get.TEMPERATE_RAIN_FOREST,get.TROPICAL_RAIN_FOREST,get.TROPICAL_SEASONAL_FOREST])):
		minerals[Mineral.type.BUILDING_MATERIAL][Mineral.building_material.TREE] = randf_range(0.4,1)
	if(site.biome in ([get.HIGHLANDS,get.SCORCHED,get.TEMPERATE_RAIN_FOREST,get.TROPICAL_RAIN_FOREST,get.TROPICAL_SEASONAL_FOREST]) && randf()>0.5):
		minerals[Mineral.type.FOSSIL_FUEL][Mineral.fossil_fuel.COAL] = randf_range(0.8,1)
	if(site.animals>0.8 && randf()>0.5):
		minerals[Mineral.type.FOSSIL_FUEL][Mineral.fossil_fuel.OIL] = randf_range(0.2,1)
	if(site.animals>0.6 && randf()>0.5):
		minerals[Mineral.type.FOSSIL_FUEL][Mineral.fossil_fuel.GAS] = randf_range(0.2,1)
	if(randf()>0.8 && site.biome in ([get.GRASSLAND,get.TEMPERATE_RAIN_FOREST,get.TROPICAL_RAIN_FOREST,get.TROPICAL_SEASONAL_FOREST,get.DESERT,get.HALF_DESERT,get.SAVANNAH])):
		minerals[Mineral.type.ORE][Mineral.ore.URANIUM] = randf_range(0.6,1)
	if(randf()>0.9 && site.biome in ([get.SCORCHED,get.HIGHLANDS,get.SANDY_BEACH,get.DESERT,get.HALF_DESERT])):
		minerals[Mineral.type.ORE][Mineral.ore.PLATINUM] = randf_range(0.1,0.6)
	if(randf()>0.7 && site.biome in ([get.SCORCHED,get.HIGHLANDS,get.SANDY_BEACH,get.DESERT,get.HALF_DESERT])):
		minerals[Mineral.type.ORE][Mineral.ore.GOLD] = randf_range(0.3,1)
	if(randf()>0.6 && site.biome in ([get.SCORCHED,get.HIGHLANDS,get.SANDY_BEACH,get.DESERT,get.HALF_DESERT])):
		minerals[Mineral.type.ORE][Mineral.ore.SILVER] = randf_range(0.3,1)
	if(randf()>0.7 && !(site.biome in ([get.SNOW,get.SWAMP,get.SANDY_BEACH]))):
		minerals[Mineral.type.ORE][Mineral.ore.ALUMINUM] = randf_range(0.3,1)
	if(randf()>0.3 && site.biome in ([get.SWAMP,get.DESERT,get.HALF_DESERT,get.SANDY_BEACH])):
		minerals[Mineral.type.ORE][Mineral.ore.SULFUR] = randf_range(0.3,1)
	if(randf()>0.6 && site.biome in ([get.SCORCHED,get.HIGHLANDS,get.SANDY_BEACH,get.DESERT,get.HALF_DESERT])):
		minerals[Mineral.type.ORE][Mineral.ore.COPPER] = randf_range(0.3,1)
	if(randf()>0.2 && site.biome in ([get.HALF_DESERT,get.DESERT,get.SANDY_BEACH])):
		minerals[Mineral.type.BUILDING_MATERIAL][Mineral.building_material.LIMESTONE] = randf_range(0.4,1)
	if(site.biome in ([get.HALF_DESERT,get.DESERT,get.SANDY_BEACH])):
		minerals[Mineral.type.BUILDING_MATERIAL][Mineral.building_material.SAND] = randf_range(0.6,1)
	if(randf()>0.3 && site.biome in ([get.TEMPERATE_RAIN_FOREST,get.TROPICAL_RAIN_FOREST,get.TROPICAL_SEASONAL_FOREST,get.SWAMP,get.GRASSLAND])):
		minerals[Mineral.type.BUILDING_MATERIAL][Mineral.building_material.CLAY] = randf_range(0.3,0.6)
	if(randf()>0.8 && site.biome in ([get.SCORCHED,get.HIGHLANDS])):
		minerals[Mineral.type.GEM][Mineral.gem.DIAMOND] = randf_range(0.3,1)
	if(randf()>0.8 && site.biome in ([get.SCORCHED,get.HIGHLANDS])):
		minerals[Mineral.type.GEM][Mineral.gem.EMERALD] = randf_range(0.3,1)
	if(randf()>0.8 && site.biome in ([get.SCORCHED,get.HIGHLANDS])):
		minerals[Mineral.type.GEM][Mineral.gem.RUBY] = randf_range(0.3,1)
	if(randf()>0.8 && site.biome in ([get.SCORCHED,get.HIGHLANDS])):
		minerals[Mineral.type.GEM][Mineral.gem.SAPPHIRE] = randf_range(0.3,1)
	return minerals


func set_biome(site:Delaunay_Bower.VoronoiSite, e:float, m:float) ->void:
	site.elevation = e
	site.moisture = m
	var biome:int = Biome.get_biome(e,m)
	site.biome = biome
	recalculate_boundary_biomes(site)
	Biome.recalculate_moisture(site)
	site.fertility = Biome.calculate_fertility(biome)
	site.drinking_water = Biome.calculate_drinking_water(biome)
	site.animals = Biome.calculate_animals(biome)
	site.minerals = calculate_minerals(site)
	site.is_land = Biome.isLand(biome)
	site.is_inhabitable_land = Biome.isInhabitableLand(site)



func set_biome_name(site:Delaunay_Bower.VoronoiSite) ->void:
	if(site.biome not in ([get.OCEAN,get.SHALLOW])):
		var province = Civilization.generate_province_name()
		while(province in existing_provinces_names):
			province = Civilization.generate_province_name()
		existing_provinces_names.append(province)
		site.province_name = Civilization.generate_province_name()
	elif(site.biome == get.OCEAN):
		site.province_name = "Непроходимые воды"
	else:
		site.province_name = "Мелководье"


func recalculate_boundary_biomes(site) ->void:
	var boundary:Rect2 = site.get_boundary()
	if(boundary.position.x < 0 || boundary.position.y < 0):
		site.biome = get.OCEAN
		return
	var screen_size_x = DisplayServer.screen_get_size().x
	var screen_size_y = DisplayServer.screen_get_size().y
	if ((boundary.position.x + boundary.size.x) > screen_size_x) || ((boundary.position.y + boundary.size.y) > screen_size_y):
		site.biome = get.OCEAN


static func recalculate_moisture(site) ->void:
	if site.biome == get.OCEAN || site.biome == get.SHALLOW:
		site.moisture = 1


func recalculate_shallow_biome_position(site:Delaunay_Bower.VoronoiSite) ->void:
	if site.biome == get.SHALLOW:
		var neighbours_count:int = 0
		var neighbours_ocean_count:int = 0
		for neighbour_edge in site.neighbours:
			neighbours_count+=1
			if neighbour_edge.other.biome in ([get.OCEAN,get.SHALLOW]):
				neighbours_ocean_count+=1
		var neighbours_ratio:float = float(neighbours_ocean_count) / float(neighbours_count)
		if (neighbours_ratio > 0.3):
			site.biome = get.OCEAN


func make_shallow_between_near_lands(site:Delaunay_Bower.VoronoiSite) ->void:
	if site.biome == get.OCEAN:
		var neighbours_count:int = 0
		var neighbours_land_count:int = 0
		for neighbour_edge in site.neighbours:
			neighbours_count+=1
			if neighbour_edge.other.biome not in ([get.OCEAN,get.SHALLOW]):
				neighbours_land_count+=1
		var neighbours_ratio:float = float(neighbours_land_count) / float(neighbours_count)
		if (neighbours_ratio > 0.5):
			site.biome = get.SHALLOW

static func get_biome(e: float, m: float) ->get:
	if(e < 0.5):
		return get.OCEAN
	if(e < 0.55):
		return get.SHALLOW
	if(e < 0.57):
		return get.SANDY_BEACH
	if(e > 0.8 && m > 0.5):
		return get.SNOW
	if(e > 0.7 && m < 0.5):
		return get.SCORCHED
	if(e > 0.7):
		return get.HIGHLANDS
	
	
	if(m > 0.9):
		return get.SWAMP
	if(m > 0.8):
		return get.TROPICAL_RAIN_FOREST
	if(m > 0.7):
		return get.TROPICAL_SEASONAL_FOREST
	if(m > 0.6):
		return get.TEMPERATE_RAIN_FOREST
	if(m < 0.25):
		return get.DESERT
	if(m < 0.3):
		return get.HALF_DESERT
	if(m < 0.35):
		return get.SAVANNAH
	return get.GRASSLAND


static func isLand(biome:int) -> bool:
	if biome in ([get.OCEAN,get.SHALLOW]):
		return false
	return true


static func isInhabitableLand(site:Delaunay_Bower.VoronoiSite) -> bool:
	if !isLand(site.biome):
		return false
	if site.moisture>0.3 && site.fertility>0.6 && site.drinking_water>0.2 && site.animals>0.2:
		return true
	return false
