class_name CivLabel
extends RichTextLabel


var id:int
var main_map:MainMapGenerator


func init(_main_map) ->void:
	main_map = _main_map


func _on_mouse_entered() ->void:
	tooltip_text = "Вывести информацию о цивилизации " + text
	text = "[u]" + text + "[/u]"


func _on_mouse_exited() ->void:
	text = text.left(-4).right(-3)


func _input(event) ->void:
	if event.is_action_pressed("left_mouse"):
		if Rect2(Vector2(), size).has_point(get_local_mouse_position()):
			if(is_instance_valid(main_map)):
				main_map.show_civ_info(id)
			return
