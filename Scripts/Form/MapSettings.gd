class_name MapSettings
extends VBoxContainer


@onready var elevation_seed_slider:HSlider = $ElevationSeedContainer/ElevationSeedSlider
@onready var points_seed_slider:HSlider = $PointsSeedContainer/PointsSeedSlider
@onready var moisture_seed_slider:HSlider = $MoistureSeedContainer/MoistureSeedSlider

@onready var elevation_octaves_slider:HSlider = $ElevationOctavesContainer/ElevationOctavesSlider
@onready var elevation_lacunarity_slider:HSlider = $ElevationLacunarityContainer/ElevationLacunaritySlider
@onready var elevation_fractal_gain_slider:HSlider = $ElevationFractalGainContainer/ElevationFractalGainSlider
@onready var elevation_frequency_slider: HSlider = $ElevationFrequencyContainer/ElevationFrequencySlider

@onready var moisture_octaves_slider:HSlider = $MoistureOctaveContainer/MoistureOctaveSlider
@onready var moisture_lacunarity_slider:HSlider = $MoistureLacunarityContainer/MoistureLacunaritySlider
@onready var moisture_frequency_slider:HSlider = $MoistureFrequencyContainer/MoistureFrequencySlider
@onready var moisture_fractal_gain_slider:HSlider = $MoistureFractalGainContainer/MoistureFractalGainSlider


func _on_moisture_frequency_slider_value_changed(value) ->void:
	moisture_frequency_slider.tooltip_text = str(value)


func _on_elevation_fractal_gain_slider_value_changed(value) ->void:
	elevation_fractal_gain_slider.tooltip_text = str(value)


func _on_elevation_frequency_slider_value_changed(value) ->void:
	elevation_frequency_slider.tooltip_text = str(value)


func _on_moisture_fractal_gain_slider_value_changed(value) ->void:
	moisture_fractal_gain_slider.tooltip_text = str(value)

func _on_elevation_seed_slider_value_changed(value):
	elevation_seed_slider.tooltip_text = str(value)


func _on_moisture_seed_slider_value_changed(value):
	moisture_seed_slider.tooltip_text = str(value)


func _on_points_seed_slider_value_changed(value):
	points_seed_slider.tooltip_text = str(value)


func _on_elevation_octaves_slider_value_changed(value):
	elevation_octaves_slider.tooltip_text = str(value)


func _on_elevation_lacunarity_slider_value_changed(value):
	elevation_lacunarity_slider.tooltip_text = str(value)


func _on_moisture_octave_slider_value_changed(value):
	moisture_octaves_slider.tooltip_text = str(value)


func _on_moisture_lacunarity_slider_value_changed(value):
	moisture_lacunarity_slider.tooltip_text = str(value)
