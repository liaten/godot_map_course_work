class_name EditSite
extends ColorRect

var screen_size_x: float = DisplayServer.screen_get_size().x
var screen_size_y: float = DisplayServer.screen_get_size().y
var edit_site:Node2D = null
var main_map:MainMapGenerator

func _input(_event) ->void:
	if not Rect2(Vector2(), $Background.size).has_point(get_local_mouse_position()):
		main_map.enable_polygons_highlight()
		main_map.allow_selecting_site()
		return
	main_map.disable_polygons_highlight()
	main_map.restrict_selecting_site()


func init(mainMap:MainMapGenerator) ->void:
	main_map = mainMap


func _on_close_button_pressed() ->void:
	if(is_instance_valid(edit_site)):
		edit_site.queue_free()


func _on_tree_exited() ->void:
	main_map.allow_all()


func _on_tree_entered() ->void:
	edit_site = get_parent()
	self.position = Vector2(screen_size_x/2-self.offset_right/2,screen_size_y/2-self.offset_bottom/2)
