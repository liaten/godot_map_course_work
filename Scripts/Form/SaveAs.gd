class_name SaveAs
extends ColorRect

var screen_size_x: float = DisplayServer.screen_get_size().x
var screen_size_y: float = DisplayServer.screen_get_size().y
var save_text:String = ""
var main_map:MainMapGenerator
var current_action:MainMapGenerator.ACTION


func init(action:MainMapGenerator.ACTION, _main_map:MainMapGenerator) ->void:
	current_action = action
	main_map = _main_map
	self.position = Vector2(screen_size_x/2-self.offset_right/2,screen_size_y/2-self.offset_bottom/2)
	main_map.set_save_as_window_active()


func _on_CloseButton_pressed() ->void:
	if is_instance_valid(get_parent()):
		get_parent().queue_free()


func _on_SaveButton_pressed() ->void:
	var regex = RegEx.new()
	regex.compile("^[a-zA-Z0-9_]+$")
	if(regex.search(save_text)==null):
		match current_action:
			main_map.ACTION.SAVE_MAP:
				main_map.add_notification_alarm("Имя карты должно содержать только символы латинского алфавита, цифры и нижнее подчёркивание, не должно содержать пробелы")
		return
	match current_action:
		main_map.ACTION.SAVE_MAP:
			var data:Dictionary = {
				"save_map_location" = Files.map_folder + save_text + ".save"
			}
			main_map.save_map_create_thread(data)
		main_map.ACTION.SAVE_CIVILIZATIONS:
			var data:Dictionary = {
				"save_civs_location" = Files.civ_folder + save_text + ".save"
			}
			main_map.save_civs_create_thread(data)
	_on_CloseButton_pressed()


func _on_LineEdit_text_changed(new_text) ->void:
	save_text = new_text


func _on_tree_exited() ->void:
	main_map.set_save_as_window_inactive()
	main_map.allow_all()
