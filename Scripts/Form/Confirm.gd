class_name Confirm
extends ColorRect

var screen_size_x: float = DisplayServer.screen_get_size().x
var screen_size_y: float = DisplayServer.screen_get_size().y
var main_map: MainMapGenerator


func init(_main_map:MainMapGenerator) ->void:
	main_map = _main_map


func _on_apply_pressed() ->void:
	main_map.show_main_menu()


func _on_close_button_pressed() ->void:
	main_map.allow_all()
	main_map.closable_windows_array.erase(get_parent())
	if is_instance_valid(get_parent()):
		get_parent().queue_free()


func _on_tree_entered() ->void:
	self.position = Vector2(screen_size_x/2-self.offset_right/2,screen_size_y/2-self.offset_bottom/2)
