extends ColorRect

var events: Array = []
var is_mouse_entered:bool = false
var edit_site_main: ColorRect
var header_event_position
var is_moving_now:bool = false

func _ready() ->void:
	edit_site_main = get_parent()
	
func _input(event) ->void:
	if is_mouse_entered:
		if event.is_action_pressed("left_mouse"):
			events.append("left_mouse")
			return
		if event.is_action_released("left_mouse"):
			events = []
			is_moving_now = false
			return
		if event is InputEventMouseMotion && events!=[] && events[0] == "left_mouse":
			is_moving_now = true
			edit_site_main.position = Vector2(event.position.x - header_event_position.x ,event.position.y - header_event_position.y)
			return


func _on_Header_mouse_entered() ->void:
	is_mouse_entered = true


func _on_Header_mouse_exited() ->void:
	is_mouse_entered = false


func _on_Header_gui_input(event) ->void:
	if !is_moving_now && event is InputEventMouseMotion:
		header_event_position = event.position
